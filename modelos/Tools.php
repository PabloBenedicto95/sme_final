<?php
class Tools{
	
	public static function crearMenuTop(){
		echo '<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="mobile-only-brand pull-left">
		<div class="nav-header pull-left">
		<div class="logo-wrap" style="padding-top: 0%;">
		<a href="index.html">
		<img class="brand-img" src="dist/img/logo.png" alt="brand" backColor="Transparent"/>
		<span class="brand-text">SME</span>
		</a>
		</div>
		</div>	
		<a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>
		<a id="toggle_mobile_search" data-toggle="collapse" data-target="#search_form" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-search"></i></a>
		<a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>
		<form id="search_form" role="search" class="top-nav-search collapse pull-left" method="post" action="busqueda.php">
		<div class="input-group">
		<input type="text" name="campo" class="form-control" placeholder="Eventos/Clientes" id="input-busqueda">
		<span class="input-group-btn">
		<button type="button" class="btn  btn-default"  data-target="#search_form" data-toggle="collapse" aria-label="Close" aria-expanded="true"><i class="zmdi zmdi-search"></i></button>
		</span>
		</div>
		</form>
		</div>
		<div id="mobile_only_nav" class="mobile-only-nav pull-right">
		<ul class="nav navbar-right top-nav pull-right">
		<li class="dropdown auth-drp">
		<a href="#" class="dropdown-toggle pr-0" data-toggle="dropdown"><img src=';
		if(isset($_SESSION['usuario'])){
			echo 'dist/img/desconocido.jpg';
		}
		echo ' alt="user_auth" class="user-auth-img img-circle"/><span class="user-online-status"></span></a>
		<ul class="dropdown-menu user-auth-dropdown" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
		';
		if(isset($_SESSION['usuario'])){
			echo '<li><a href="usuario.php?id='.$_SESSION['id'].'"><i class="zmdi zmdi-account"></i><span>'.$_SESSION['usuario'].'</span></a></li><li class="divider"></li>';
		}
		echo '
		<li>
		<a href=';
		if(isset($_SESSION['usuario'])){
			echo '"./logout.php"';
		}else{
			echo '"./login.php"';
		}
		echo '><i class=';
		if(isset($_SESSION['usuario'])){
			echo '"fa fa-sign-out"';
		}else{
			echo '"fa fa-sign-in"';
		}
		echo '></i><span>';
		if(isset($_SESSION['usuario'])){
			echo 'Desconectarse';
		}else{
			echo 'Iniciar sesión';
		}
		echo '</span></a>
		</li>
		</ul>
		</li>
		</ul>
		</div>	
		</nav>';
	}

	public static function crearMenuLeft(){
		$enlace=self::conexion();

		echo '<div class="fixed-sidebar-left">
		<ul class="nav navbar-nav side-nav nicescroll-bar">
		<li class="navigation-header" id="link_cliente" target="_blank">';

		if(!isset($_SESSION['admin'])){
			$cliente_cache=mysqli_fetch_assoc(mysqli_query($enlace, 'SELECT * FROM sme_clientes WHERE ID='.$_SESSION['id_cliente']));
			echo '<span>'.$cliente_cache['nombre'].'</span>';
		}

		echo '<i class="zmdi zmdi-more"></i>
		</li>';
		if(!isset($_SESSION['admin'])){
			echo '<script type="text/javascript">
			document.getElementById("link_cliente").onclick=function(){location.href="cliente.php?id='.$_SESSION['id_cliente'].'"};
			document.getElementById("link_cliente").style.cursor="pointer";
			document.getElementById("link_cliente").title="Acceso al cliente";
			</script>';
		}
		echo '<li>
		<a class="active" href="./" data-toggle="collapse" data-target="#dashboard_dr"><div class="pull-left" title="';

		if(isset($_SESSION['admin'])){
			echo 'Ver todos los eventos';
		}else{
			echo 'Ver todos los eventos de '.$cliente_cache['nombre'];
		}

		echo '"><i class="fa fa-fort-awesome"></i><span class="right-nav-text" style="text-shadow: none; color: white;">&nbsp;&nbsp;';

		if(isset($_SESSION['admin'])){
			echo "Eventos";
		}else{
			echo 'Mis eventos';
		}

		echo '</span></div><div class="pull-right"><span class="label label-warning" id="alertas-menu-inicio" title="Nuevos eventos"></span></div><div class="clearfix"></div></a>
		</li>

		';

		if(isset($_SESSION['admin'])){
			$consulta=mysqli_query($enlace, 'select * from sme_clientes');
			$consulta=mysqli_fetch_array($consulta);
			if(count($consulta)!=0){
				echo '<li>
				<a href="listado_clientes.php" data-toggle="collapse" data-target="#app_dr" style="color: white;"><div class="pull-left"><i class="fa fa-group"></i><span class="right-nav-text">&nbsp;&nbsp;Clientes</span></div><div class="pull-right"></div><div class="clearfix"></div></a>';
			}
		}
		echo '</ul>
		</div>';
	}

	public static function crearFooter(){
		if(isset($_SESSION['admin'])){
			self::crearModalClientes(self::conexion());
		}
		echo '<footer class="footer container-fluid pl-30 pr-30">
		<div class="row">
		<div class="col-sm-12">
		<p>2018 &copy; SME. Powered by <a href="https://www.casfid.es/" target="_blank">CASFID</a></p>
		</div>
		</div>
		</footer>';
	}

	private static function crearModalClientes($conexion){
		echo '<!-- sample modal content -->
		<div id="modal_clientes" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
		<div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h5 class="modal-title" id="myModalLabel">Clientes</h5>
		</div>
		<div class="modal-body">
		<h5 class="mb-15">Listado de todos los clientes</h5>
		<div class="panel-group accordion-struct" id="accordion_1" role="tablist" aria-multiselectable="true">';

		$consulta=mysqli_query($conexion, 'SELECT * FROM sme_clientes');

		$contador=1;
		while($fila=mysqli_fetch_array($consulta)){
			echo '<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="heading_'.$contador.'">
			<a role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapse_'.$contador.'" aria-expanded="false" class="collapsed">'.$fila['nombre'].'</a> 
			</div>
			<div id="collapse_'.$contador.'" class="panel-collapse collapse" role="tabpanel" aria-expanded="false" style="height: 0px;">
			<div class="panel-body pa-15">';
			$usuarios_consulta=mysqli_query($conexion, 'SELECT * FROM sme_usuarios WHERE id_cliente='.$fila['ID']);

			if(mysqli_num_rows($usuarios_consulta)!=0){
				echo '<div class="table-responsive"><table class="table table-hover table-bordered mb-0"><tbody>';
				while($usuario_datos=mysqli_fetch_array($usuarios_consulta)){
					echo '<tr><td>'.$usuario_datos['nombre'].'</td><td class="text-nowrap"><a href="usuario.php?id='.$usuario_datos['ID'].'" class="mr-25" data-toggle="tooltip" data-original-title="Editar"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a> <a href="borrar_usuario.php?id='.$usuario_datos['ID'].'" data-toggle="tooltip" data-original-title="Borrar"> <i class="fa fa-close text-danger"></i></a></td></tr>';
				}
				echo '</tbody></table></div>';
			}
			echo '</div>
			</div>
			</div>';
			$contador++;
		}
		echo '</div>
		<div class="modal-footer">
		<button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
		</div>
		</div>
		<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
		</div>
		</div>
		<!-- /.modal -->';
	}

	public static function paginaAnterior(){
		if(isset($_SESSION['cache_url'])){
			$_SESSION['registro_rapido']=$_SESSION['cache_url'];
			unset($_SESSION['cache_url']);
		}
	}

	public static function crearHeaderYConexion(){

		if(!isset($_SESSION['usuario'])){
			header('Location: login.php');
		}

		if(isset($_SESSION['registro_rapido'])){
			$envio=$_SESSION['registro_rapido'];
			unset($_SESSION['registro_rapido']);
			header('Location: '.$envio);
		}


		$enlace=self::conexion();


		echo '<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta name="description" content="Jetson is a Dashboard & Admin Site Responsive Template by hencework." />
		<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Jetson Admin, Jetsonadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
		<meta name="author" content="hencework"/>

		<!-- Favicon -->
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="icon" href="favicon.ico" type="image/x-icon">

		<!-- Morris Charts CSS -->
		<link href="vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>

		<!-- vector map CSS -->
		<link href="vendors/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" type="text/css"/>

		<!-- Calendar CSS -->
		<link href="vendors/bower_components/fullcalendar/dist/fullcalendar.css" rel="stylesheet" type="text/css"/>

		<!-- Data table CSS -->
		<link href="vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
		
		<!-- Data table CSS -->
		<link href="vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
		
		<link href="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">
		
		<!-- Custom CSS -->
		<link href="dist/css/style.css" rel="stylesheet" type="text/css">

		<style type="text/css">

		
		</style>';

		//Devuelve el enlace
		return $enlace;
	}

	public static function crearHeaderSinConexion(){
		echo '<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<meta name="description" content="Jetson is a Dashboard & Admin Site Responsive Template by hencework." />
		<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Jetson Admin, Jetsonadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
		<meta name="author" content="hencework"/>

		<!-- Favicon -->
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="icon" href="favicon.ico" type="image/x-icon">

		<!-- Morris Charts CSS -->
		<link href="vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>

		<!-- vector map CSS -->
		<link href="vendors/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" type="text/css"/>

		<!-- Calendar CSS -->
		<link href="vendors/bower_components/fullcalendar/dist/fullcalendar.css" rel="stylesheet" type="text/css"/>

		<!-- Data table CSS -->
		<link href="vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
		
		<!-- Data table CSS -->
		<link href="vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
		
		<link href="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">
		
		<!-- Custom CSS -->
		<link href="dist/css/style.css" rel="stylesheet" type="text/css">

		<style type="text/css">

		
		</style>';
	}

	public static function conexion(){
		error_reporting(0);
		define('_DB_USER_', 'root');

		define('_DB_PASSWD_', 'casfid2014');

		define('_DB_SERVER_', 'localhost');
		error_reporting(-1);

		$enlace=mysqli_connect(_DB_SERVER_, _DB_USER_, _DB_PASSWD_, "casfid");
		mysqli_query($enlace, "SET NAMES utf8");

		return $enlace;
	}

	public static function crearJavascript(){
		echo '<!--JavaScript personal-->

		<!-- jQuery -->
		<script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>

		<!-- Bootstrap Core JavaScript -->
		<script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="dist/js/modal-data.js"></script>

		<!-- Data table JavaScript -->
		<script src="vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>

		<!-- Slimscroll JavaScript -->
		<script src="dist/js/jquery.slimscroll.js"></script>

		<!-- Progressbar Animation JavaScript -->
		<script src="vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
		<script src="vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>

		<!-- Fancy Dropdown JS -->
		<script src="dist/js/dropdown-bootstrap-extended.js"></script>

		<!-- Sparkline JavaScript -->
		<script src="vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>

		<!-- Owl JavaScript -->
		<script src="vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>

		<!-- Switchery JavaScript -->
		<script src="vendors/bower_components/switchery/dist/switchery.min.js"></script>

		<!-- EChartJS JavaScript -->
		<script src="vendors/bower_components/echarts/dist/echarts-en.min.js"></script>
		<script src="vendors/echarts-liquidfill.min.js"></script>

		<!-- Toast JavaScript -->
		<script src="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>

		<!-- Init JavaScript -->
		<script src="dist/js/init.js"></script>';
	}

	public static function preloader(){
		echo '<div class="preloader-it" style="background: #004899; display:flex; align-items:center;">
		<div class="la-anim-1"></div>
		<img src="dist/img/loading.gif" style="width: 25%; margin-left: auto; margin-right: auto; display: block;">
		</div>';
		flush();
		ob_flush();
	}

	public static function cartel_eventos($la_consulta){
		error_reporting(1);
		$meses = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');

		$contador=0;

		while($fila=mysqli_fetch_array($la_consulta)){
			if(file_get_contents($fila['imagen'])){
				$imagen='background-image: url(\''.$fila['imagen'].'\');';
			}else{
				$imagen='';
			}

			$array=str_split($fila['fecha']);

			$fecha=$array[8].$array[9].' de ';

			if($array[5]!='0'){
				$fecha.=$meses[(intval($array[6])+10)-1].' del ';
			}else{
				$fecha.=$meses[(intval($array[6]))-1].' del ';
			}

			$fecha.=$array[0].$array[1].$array[2].$array[3];



			echo '<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12 cuadro_evento" style="float: left;">
			<div class="panel panel-default card-view panel-refresh panel-de-imagen cartel_evento" style="'.$imagen.'">
			<div class="refresh-container">
			<div class="la-anim-1"></div>
			</div>
			<div class="panel-heading">
			<a href="evento.php?id='.$fila['ID'].'">
			<div class="pull-left titulo">
			<h6 class="panel-title txt-dark titulo_final" style="text-overflow: ellipsis;" title="'.$fila['nombre'].'">'.$fila['nombre'].'</h6>
			</div>
			</a>
			<div class="pull-right">
			<div class="pull-left inline-block dropdown">
			</div>
			</div>
			<div class="clearfix"></div>
			</div>
			<a href="evento.php?id='.$fila['ID'].'">
			<div class="tamano_imagen"><span class="fechas">'
			.$fecha.'</span>
			</div>
			</a>
			</div>
			</div>';
			$contador++;
		}	
		error_reporting(-1);
		return $contador;
	}

	public static function formulario_modificar_evento($numero, $enlace){
		echo '<button class="btn btn-warning btn-icon-anim btn-square" title="Modificar el evento" data-toggle="modal" data-target="#exampleModal" style="position: fixed; right: 2%; bottom: 5%; z-index: 999;">
		<li class="fa fa-pencil">
		</button>
		</a>

		<!--Formulario de modificación-->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
		<div class="modal-dialog" role="document">
		<div class="modal-content">
		<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h5 class="modal-title" id="exampleModalLabel1">'.$_SESSION['evento']['nombre'].'</h5>
		</div>
		<div class="modal-body">
		<form method="post" action="modificar_evento.php" id="datos_nuevos">';

		echo '
		<div class="form-group">
		<label for="inputName" class="control-label mb-10">Nombre(*)</label>
		<input type="text" class="form-control" id="input_nombre" placeholder="Nombre" name="nombre" required ';
		if($_SESSION['evento']['nombre']!=''){
			echo 'value="'.$_SESSION['evento']['nombre'].'"';
		}
		echo '>
		</div>

		<div class="form-group">
		<label for="inputName" class="control-label mb-10">Nombre del sms</label>
		<input type="text" class="form-control" id="input_nombre_sms"  name="nombre_sms" placeholder="SMS" ';
		if($_SESSION['evento']['nombre_sms']!=''){
			echo 'value="'.$_SESSION['evento']['nombre_sms'].'"';
		}
		echo '>
		</div>

		<div class="form-group">
		<label for="inputName" class="control-label mb-10">Link de la imagen</label>
		<input type="text" class="form-control" id="input_imagen"  name="imagen" placeholder="Imagen" ';
		if($_SESSION['evento']['imagen']!=''){
			echo 'value="'.$_SESSION['evento']['imagen'].'"';
		}
		echo '>
		</div>

		<div class="form-group">
		<label for="inputName" class="control-label mb-10">Lugar</label>
		<input type="text" class="form-control" id="input_lugar"  name="lugar" placeholder="Lugar" ';
		if($_SESSION['evento']['lugar']!=''){
			echo 'value="'.$_SESSION['evento']['lugar'].'"';
		}
		echo '>
		</div>

		<div class="form-group">
		<label for="inputName" class="control-label mb-10">Fecha y hora</label>
		<input type="datetime-local" class="form-control fecha" id="input_fecha"  name="fecha" title="Introduce la fecha y la hora" style="width: 50%" ';
		if($_SESSION['evento']['fecha']!=''){
			$string=str_split($_SESSION['evento']['fecha']);

			for($i=0; $i<count($string); $i++){
				if($string[$i]==' '){
					$string[$i]='T';
				}
			}

			echo 'value="'.implode($string).'"';
		}
		echo '>
		</div>

		<div class="form-group">
		<label for="inputName" class="control-label mb-10">Mostrar comprobar pulsera</label>
		<input type="checkbox" class="js-switch js-switch-1" data-color="#2ECC40" id="input_mostrar_comprobar_pulsera"  name="mostrar_comprobar_pulsera" style="width: 5%;" ';
		if($_SESSION['evento']['mostrar_comprobar_pulsera']){
			echo 'data-switchery="true" checked';
		}
		echo '>
		</div>

		<div class="form-group">
		<label for="inputName" class="control-label mb-10">Registro previo</label>
		<input type="checkbox" class="js-switch js-switch-1" data-color="#2ECC40" id="input_registro_previo"  name="registro_previo" style="width: 5%;" ';
		if($_SESSION['evento']['registro_previo']){
			echo 'data-switchery="true" checked';
		}
		echo '>
		</div>

		<div class="form-group">
		<label for="inputName" class="control-label mb-10">Registro por email</label>
		<input type="checkbox" class="js-switch js-switch-1" data-color="#2ECC40" id="input_registro_email"  name="registro_email" style="width: 5%;" ';
		if($_SESSION['evento']['registro_email']){
			echo 'data-switchery="true" checked';
		}
		echo '>
		</div>

		<div class="form-group">
		<label for="inputName" class="control-label mb-10">Registro telefónico</label>
		<input type="checkbox" class="js-switch js-switch-1" data-color="#2ECC40" id="input_registro_telefono"  name="registro_telefono" style="width: 5%;" ';
		if($_SESSION['evento']['registro_telefono']){
			echo 'data-switchery="true" checked';
		}
		echo '>
		</div>

		
		<div class="form-group">
		<label for="inputName" class="control-label mb-10">Id del lugar de Facebook</label>
		<input type="text" class="form-control" id="input_id_lugar_fb"  name="id_lugar_fb" placeholder="Id de fb" ';
		if($_SESSION['evento']['id_lugar_fb']!=''){
			echo 'value="'.$_SESSION['evento']['id_lugar_fb'].'"';
		}
		echo '>
		</div>

		<div class="form-group">
		<label for="inputName" class="control-label mb-10">Página de Facebook</label>
		<input type="text" class="form-control" id="input_facebook_pagina"  name="facebook_pagina" placeholder="Página facebook" ';
		if($_SESSION['evento']['facebook_pagina']!=''){
			echo 'value="'.$_SESSION['evento']['facebook_pagina'].'"';
		}
		echo '>
		</div>

		<div class="form-group">
		<label for="inputName" class="control-label mb-10">URL</label>
		<input type="text" class="form-control" id="input_url"  name="url" placeholder="url" ';
		if($_SESSION['evento']['url']!=''){
			echo 'value="'.$_SESSION['evento']['url'].'"';
		}
		echo '>
		</div>
		';

		echo '</div>
		<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		<button type="button" class="btn btn-primary" alt="alert" id="sa-warning-evento">Aplicar cambios</button>
		</div>
		</form>
		</div>
		</div>
		</div>
		';
	}

	public static function crearCookieNumero($numero){
		echo '<script type="text/javascript">
		document.cookie=\'retorno=\'+window.location.href;
		document.cookie=\'numero=';
		echo $numero;
		echo '\';</script>';
	}

	public static function encriptar($string){
		$partido=str_split($string);
		$nuevo='';
		$y=getdate()['seconds'];

		for($i=0; $i<count($partido); $i++){
			$x=(ord($partido[$i])/8)*$y;
			$nuevo.=$x.'*/';
		}

		$nuevo.=$y;

		return $nuevo;
	}

	public static function desencriptar($string){
		$partido=explode('*/', $string);
		$nuevo='';
		$y=$partido[count($partido)-1];

		for($i=0; $i<(count($partido)-1); $i++){
			$x=($partido[$i]/$y)*8;
			$nuevo.=chr($x);
		}

		return $nuevo;
	}

	public static function comprobarMismoCliente($id){
		$enlace=self::conexion();
		if($id!=$_SESSION['id']){
			$esta=false;
			$consulta=mysqli_query($enlace, 'SELECT * FROM sme_usuarios WHERE id_cliente='.$_SESSION['id_cliente']);
			while($fila=mysqli_fetch_array($consulta)){
				if($id==$fila['ID']){
					$esta=true;
				}
			}

			if(!$esta and !isset($_SESSION['admin'])){
				header('Location: ./');
			}
		}
	}

	public static function ordenarSegundos($array, $posicion, $dato){
		$valor=strtotime($dato['hora']);
		if(isset($array[$posicion])){
			if($array[$posicion]>$valor){
				$posicion=$posicion-1;
				self::ordenarSegundos($array, $posicion, $dato);
			}else if($array[$posicion]<$valor){
				$posicion=$posicion+1;
				self::ordenarSegundos($array, $posicion, $dato);
			}
		}else{
			$array[$posicion]=$valor;
		}
	}

	public static function cartelClientes($consulta){
		echo '<div class="row">';
		while($fila=mysqli_fetch_array($consulta)){
			echo '<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
						<div class="panel panel-default card-view pa-0 cartel-clientes">
							<div class="panel-wrapper collapse in">
								<div class="panel-body pa-0">
									<article class="col-item">
										<div class="photo">
											<a href="cliente.php?id='.$fila['ID'].'"> <img src="dist/img/desconocido.jpg" class="img-responsive" alt="Product Image"> </a>
										</div>
										<div class="info">
											<h6 title="'.$fila['nombre'].'">'.$fila['nombre'].'</h6>
											<span class="head-font block text-success font-16"><i class="fa fa-fort-awesome"></i> ';
											$sub_consulta=mysqli_query(self::conexion(), 'SELECT COUNT(*) FROM sme_eventos WHERE id_cliente='.$fila['ID']);
											 $total_eventos=mysqli_fetch_assoc($sub_consulta)['COUNT(*)'];
											 if($total_eventos==0){
											 	echo 'Sin eventos';
											 }else if($total_eventos==1){
											 	echo '1 evento';
											 }elseif($total_eventos>1){
											 	echo $total_eventos.' eventos';
											 }
											echo '</span>
											<span class="head-font block text-danger font-16"><i class="fa fa-group"></i> ';
											$sub_consulta=mysqli_query(self::conexion(), 'SELECT COUNT(*) FROM sme_usuarios WHERE id_cliente='.$fila['ID']);
											 $total_usuarios=mysqli_fetch_assoc($sub_consulta)['COUNT(*)'];
											 if($total_usuarios==0){
											 	echo 'Sin usuarios';
											 }else if($total_usuarios==1){
											 	echo '1 usuario';
											 }elseif($total_usuarios>1){
											 	echo $total_usuarios.' usuarios';
											 }
											echo '</span>
										</div>
									</article>
								</div>
							</div>	
						</div>	
					</div>';
		}
		echo '</div>';
	}
}
?>