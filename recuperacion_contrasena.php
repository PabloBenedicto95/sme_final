<!DOCTYPE html>
<?php
session_start();

if(isset($_COOKIE['fallo_email'])){
	$fallo_email=$_COOKIE['fallo_email'];
	setcookie("fallo_email","",(-1),"/");
}
?>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>Recuperación de contraseña I Android SME</title>
	<meta name="description" content="Jetson is a Dashboard & Admin Site Responsive Template by hencework." />
	<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Jetson Admin, Jetsonadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
	<meta name="author" content="hencework"/>

	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="icon" href="favicon.ico" type="image/x-icon">

	<!-- vector map CSS -->
	<link href="vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>

	<!-- Toast -->
	<link href="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">

	<!-- Custom CSS -->
	<link href="dist/css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<!--Preloader-->
	<div class="preloader-it">
		<div class="la-anim-1"></div>
	</div>
	<!--/Preloader-->

	<div class="wrapper pa-0">
		<!-- Main Content -->
		<div class="page-wrapper pa-0 ma-0 auth-page">
			<div class="container-fluid">
				<!-- Row -->
				<div class="table-struct full-width full-height">
					<div class="table-cell vertical-align-middle auth-form-wrap">
						<div class="auth-form  ml-auto mr-auto no-float">
							<div class="row">
								<div class="col-sm-12 col-xs-12">
									<div class="sp-logo-wrap text-center pa-0 mb-30">
										<a href="index.html">
											<img class="brand-img mr-10" src="dist/img/logo.png" alt="brand"/>
											<span class="brand-text">Social Media Engagement</span>
										</a>
									</div>
									<div class="mb-30">
										<h3 class="text-center txt-dark mb-10">¿Problemas de inicio de sesión?</h3>
										<h6 class="text-center txt-grey nonecase-font">Introduzca su email y verificaremos si está registrado. Luego enviaremos un e-mail con su nueva contraseña.</h6>
									</div>	
									<div class="form-wrap">
										<form action="resetear_contrasena.php" method="post">
											<div class="form-group">
												<script type="text/javascript">
													function comprobar(evento){
														var input=evento.target;

														if(input.value.split("").length==0){
															input.style.borderColor="red";
														}else{
															input.style.borderColor="";
														}
													}
												</script>
												<label class="control-label mb-10" for="exampleInputEmail_2">Dirección e-mail</label>
												<input type="email" class="form-control" required="" id="input_email" placeholder="Introduzca su e-mail" name="email" reuqired>
												<?php
												if(isset($fallo_email)){
													echo '<script type="text/javascript">
													document.getElementById("input_email").oninput=comprobar;
													document.getElementById("input_email").style.borderColor="'.$fallo_email.'";
													document.getElementById("input_email").placeholder="No hemos encontrado este e-mail";
													</script>';
												}
												?>
											</div>

											<div class="form-group text-center">
												<button type="submit" class="btn btn-info btn-rounded">Comprobar</button>
											</div>
										</form>
									</div>
								</div>	
							</div>
						</div>
					</div>
				</div>
				<!-- /Row -->	
			</div>

		</div>
		<!-- /Main Content -->
	</div>
	<!-- /#wrapper -->

	<!-- JavaScript -->

	<!-- jQuery -->
	<script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="vendors/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
	<script src="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>

	<!-- Slimscroll JavaScript -->
	<script src="dist/js/jquery.slimscroll.js"></script>

	<!-- Init JavaScript -->
	<script src="dist/js/init.js"></script>
	<?php
	if(isset($_SESSION['email_enviado'])){
		unset($_SESSION['email_enviado']);
		echo '<script src="dist/js/toast_envio_email.js"></script>';
	}
	?>
</body>
</html>
