<!DOCTYPE html>
<html lang="en">
<head>
	<?php
	session_start();
	include 'modelos/Tools.php';

	$enlace=Tools::crearHeaderYConexion();

	if(isset($_POST['campo'])){
		$evento=mysqli_real_escape_string($enlace, filter_var($_POST['campo'], FILTER_SANITIZE_EMAIL));
	}else{
		header('Location: ./');
	}
	?>
	<title><?php
	echo $evento;
	?> I Resultados</title>
	<style type="text/css">

	.user-online-status{
		<?php
		if(isset($_SESSION['usuario'])){
			echo 'background: green !important;';
		}else{
			echo 'display: none;';
		}
		?>
	}

	.panel-title{
		overflow:hidden;
		white-space:nowrap;
		text-overflow: ellipsis;
	}

	.titulo{
		max-width: 90%;
	}
</style>
</head>

<body>
	<!-- Preloader -->
	<?php
	Tools::preloader();
	?>
	<!-- /Preloader -->
	<div class="wrapper theme-1-active pimary-color-blue">
		<!-- Top Menu Items -->
		<?php
		Tools::crearMenuTop();
		?>
		<!-- /Top Menu Items -->
		
		<!-- Left Sidebar Menu -->
		<?php
		Tools::crearMenuLeft();
		?>
		<!-- /Left Sidebar Menu -->
		

		<!-- Main Content -->
		<div class="page-wrapper">
			<div class="container-fluid pt-25">

				<?php
				$expresion=strtolower($evento);

				if(isset($_SESSION['admin'])){
					$consulta=mysqli_query($enlace, 'SELECT * FROM sme_eventos WHERE nombre REGEXP \''.$expresion.'\'');
				}else{
					$consulta=mysqli_query($enlace, 'SELECT * FROM sme_eventos WHERE nombre REGEXP \''.$expresion.'\' and id_cliente='.$_SESSION['id']);
				}

				if(Tools::cartel_eventos($consulta)==0){
					echo 'Sin eventos';
				}

				if(isset($_SESSION['admin'])){
					$consulta=mysqli_query($enlace, 'SELECT * FROM sme_clientes WHERE nombre REGEXP \''.$expresion.'\'');
					if(mysqli_num_rows($consulta)==0){
						echo 'Sin usuarios';
					}else{
						Tools::cartelClientes($consulta);
					}
				}
				?>
			</div>

			<!-- Footer -->
			<?php
			Tools::crearFooter();
			?>
			<!-- /Footer -->


		</div>
		<!-- /Main Content -->

	</div>
	<!-- /#wrapper -->
	
	<!-- JavaScript -->
	<?php
	Tools::crearJavascript();
	?>
</body>

</html>
