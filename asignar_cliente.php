<?php
session_start();

if(!isset($_SESSION['usuario']) or !isset($_POST['evento_id']) or !isset($_GET['id'])){
	header('Location: ./');
}

include('modelos/Tools.php');
$enlace = Tools::conexion();

$evento_id=mysqli_real_escape_string($enlace, filter_var($_POST['evento_id'], FILTER_SANITIZE_EMAIL));
$cliente_id=mysqli_real_escape_string($enlace, filter_var($_GET['id'], FILTER_SANITIZE_EMAIL));

if(!mysqli_query($enlace, 'UPDATE sme_eventos SET id_cliente='.$cliente_id.' WHERE ID='.$evento_id)){
	echo '<script type="text/javascript">alert(\'Fallo en la asignación\');</script>';
}

Tools::paginaAnterior();

header('Location: cliente.php?id='.$cliente_id);

?>