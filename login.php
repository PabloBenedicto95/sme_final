<!DOCTYPE html>
<html lang="en">
<head>
	<?php
	include_once('modelos/Tools.php');
	?>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>SME APP I Android SME</title>
	<meta name="description" content="Jetson is a Dashboard & Admin Site Responsive Template by hencework." />
	<meta name="keywords" content="admin, admin dashboard, admin template, cms, crm, Jetson Admin, Jetsonadmin, premium admin templates, responsive admin, sass, panel, software, ui, visualization, web app, application" />
	<meta name="author" content="hencework"/>
	
	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	
	<!-- vector map CSS -->
	<link href="vendors/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>
	
	<!-- Custom CSS -->
	<link href="dist/css/style.css" rel="stylesheet" type="text/css">

	<!-- Morris Charts CSS -->
	<link href="vendors/bower_components/morris.js/morris.css" rel="stylesheet" type="text/css"/>

	<!-- vector map CSS -->
	<link href="vendors/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" type="text/css"/>

	<!-- Calendar CSS -->
	<link href="vendors/bower_components/fullcalendar/dist/fullcalendar.css" rel="stylesheet" type="text/css"/>

	<!-- Data table CSS -->
	<link href="vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>

	<!-- Data table CSS -->
	<link href="vendors/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>

	<link href="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">
	<?php
	if(isset($_COOKIE['error_log'])){
		$error_log=1;
		setcookie("error_log","",(-1),"/");
	}
	?>
</head>
<body>
	<div class="wrapper pa-0">
		<header class="sp-header">
			<div class="sp-logo-wrap pull-left">
				<a href="index.html">
					<img class="brand-img mr-10" src="dist/img/logo.png" alt="brand"/>
					<span class="brand-text">Social Media Engagement</span>
				</a>
			</div>
			<div class="form-group mb-0 pull-right">
					<span class="inline-block pr-10">¿No eres nuestro cliente?</span>
					<a class="inline-block btn btn-info  btn-rounded btn-outline" href="registrarse.php">Regístrate</a>
				</div>
		</header>

		<!-- Main Content -->
		<div class="page-wrapper pa-0 ma-0 auth-page">
			<div class="container-fluid">
				<!-- Row -->
				<div class="table-struct full-width full-height">
					<div class="table-cell vertical-align-middle auth-form-wrap">
						<div class="auth-form  ml-auto mr-auto no-float">
							<div class="row">
								<div class="col-sm-12 col-xs-12">
									<div class="mb-30">
										<h3 class="text-center txt-dark mb-10">Accede a tu cuenta</h3>
										<h6 class="text-center nonecase-font txt-grey">Introduce tus datos</h6>
									</div>	
									<div class="form-wrap">
										<form method="post" action="validar.php">
											<div class="form-group">
												<label class="control-label mb-10" for="exampleInputName_1">Usuario</label>
												<input type="text" class="form-control" required="" id="exampleInputName_1" placeholder="Ej: persona" name="nombre">
											</div>
											<div class="form-group">
												<label class="pull-left control-label mb-10" for="exampleInputpwd_2">Contraseña</label>
												<a class="capitalize-font txt-primary block mb-10 pull-right font-12" href="recuperacion_contrasena.php">¿Ha perdido su contraseña?</a>
												<div class="clearfix"></div>
												<input type="password" class="form-control" required="" id="exampleInputpwd_2" placeholder="Contraseña" name="contrasena">
											</div>
											
											<div class="form-group text-center">
												<button type="submit" class="btn btn-info btn-rounded" style="background-color: #004899; color: white; border-color: #2F57A3">Conectar</button>
											</div>
										</form>
									</div>
								</div>	
							</div>
						</div>
					</div>
				</div>
				<!-- /Row -->	
			</div>
			
			<!-- Footer -->
			<?php
			Tools::crearFooter();
			?>
			<!-- /Footer -->
		</div>
		<!-- /Main Content -->
		
	</div>
	<!-- /#wrapper -->
	
	<!-- JavaScript -->
	
	<!-- jQuery -->
	<script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>

	<!-- Slimscroll JavaScript -->
	<script src="dist/js/jquery.slimscroll.js"></script>
	
	<!-- Fancy Dropdown JS -->
	<script src="dist/js/dropdown-bootstrap-extended.js"></script>

	<!-- Owl JavaScript -->
	<script src="vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>
	
	<!-- Switchery JavaScript -->
	<script src="vendors/bower_components/switchery/dist/switchery.min.js"></script>
	
	<!-- Init JavaScript -->
	<script src="dist/js/init.js"></script>
	<?php
	if(isset($error_log)){
		echo '<script src="dist/js/toast-data.js"></script>';
		unset($error_log);
	}
	?>
</body>
</html>
