<!DOCTYPE html>
<html lang="en">
<head>
	<?php
	session_start();

	include 'modelos/Tools.php';

	Tools::crearHeaderSinConexion();

	if(isset($_COOKIE['fallo_nombre'])){
		$fallo_nombre=$_COOKIE['fallo_nombre'];
		setcookie("fallo_nombre","",(-1),"/");
	}
	if(isset($_COOKIE['fallo_telefono'])){
		$fallo_telefono=$_COOKIE['fallo_telefono'];
		setcookie("fallo_telefono","",(-1),"/");
	}
	if(isset($_COOKIE['fallo_email'])){
		$fallo_email=$_COOKIE['fallo_email'];
		setcookie("fallo_email","",(-1),"/");
	}
	if(isset($_COOKIE['fallo_usuario'])){
		$fallo_usuario=$_COOKIE['fallo_usuario'];
		setcookie("fallo_usuario","",(-1),"/");
	}
	if(isset($_COOKIE['fallo_usuario_contrasena'])){
		$fallo_usuario_contrasena=$_COOKIE['fallo_usuario_contrasena'];
		setcookie("fallo_usuario_contrasena","",(-1),"/");
	}

	$_SESSION['cache_url']=(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

	?>
	<title>Crear un nuevo cliente</title>
</head>

<body>
	<!--Preloader-->
	<?php
	Tools::preloader();
	?>
	<!--/Preloader-->
	<div class="wrapper theme-1-active pimary-color-blue">
		<header class="sp-header">
			<div class="sp-logo-wrap pull-left">
				<a href="index.html">
					<img class="brand-img mr-10" src="dist/img/logo.png" alt="brand">
					<span class="brand-text">Social Media Engagement</span>
				</a>
			</div>
		</header>
		<!-- Main Content -->
		<div class="page-wrapper">
			<div class="container-fluid pt-25">
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default card-view">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark">Nuevo cliente</h6>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">

									<div class="row">
										<div class="col-md-12">
											<div class="form-wrap">
												<form data-toggle="validator" role="form" method="post" action="introducir_cliente.php">
													<?php
													echo '<script type="text/javascript">
													function comprobar(evento){
														var input=evento.target;

														if(input.value.split("").length==0){
															input.style.borderColor="red";
															}else{
																input.style.borderColor="";
															}
														}
														</script>';
														?>
														<div class="form-group">
															<label for="inputName" class="control-label mb-10">Nombre(*)</label>
															<input type="text" class="form-control" id="input_nombre" placeholder="Nombre" name="nombre" required>
														</div>
														<?php
														if(isset($fallo_nombre)){
															echo '<script type="text/javascript">
															document.getElementById("input_nombre").oninput=comprobar;
															document.getElementById("input_nombre").style.borderColor="'.$fallo_nombre.'";
															</script>';
														}
														?>
														<div class="form-group">
															<label for="inputName" class="control-label mb-10">Teléfono</label>
															<input type="text" class="form-control" id="telefono" name="telefono" placeholder="Teléfono">
														</div>
														<script type="text/javascript">
															document.getElementById('telefono').oninput=cambioNumero;

															function cambioNumero(evento){
																var input=evento.target;

																input.value=parseInt(input.value);
																if(input.value=='NaN'){
																	input.value='';
																}

																var array=input.value.split('');

																if(array.length==9){
																	var mostrar='';
																	for(var i=0; i<3; i++){
																		mostrar+=array[i];
																	}
																	mostrar+='';

																	for(var i=3; i<9; i++){
																		mostrar+=array[i];
																		if(i==5){
																			mostrar+='';
																		}
																	}
																	input.value=mostrar;
																}else if(array.length>9){
																	var mostrar='';
																	for(var i=0; i<9; i++){
																		mostrar+=array[i];
																	}
																	input.value=mostrar;
																	cambioNumero(evento);
																}
															}
														</script>
														<?php
														if(isset($fallo_telefono)){
															echo '<script type="text/javascript">
															document.getElementById("telefono").oninput=comprobar;
															document.getElementById("telefono").style.borderColor="'.$fallo_telefono.'";
															</script>';
														}
														?>
														<div class="form-group">
															<label for="inputName" class="control-label mb-10">E-mail</label>
															<input type="email" class="form-control" id="input_email"  name="email" placeholder="E-mail">
														</div>
														<?php
														if(isset($fallo_email)){
															echo '<script type="text/javascript">
															document.getElementById("input_email").oninput=comprobar;
															document.getElementById("input_email").style.borderColor="'.$fallo_email.'";
															</script>';
														}
														?>
														<div class="form-group">
															<label for="inputName" class="control-label mb-10">Usuario inicial(*)</label>
															<input class="form-control" id="input_usuario"  name="usuario" placeholder="Usuario" required>
														</div>
														<?php
														if(isset($fallo_usuario)){
															echo '<script type="text/javascript">
															document.getElementById("input_usuario").oninput=comprobar;
															document.getElementById("input_usuario").style.borderColor="'.$fallo_usuario.'";
															</script>';
														}
														?>
														<div class="form-group">
															<label for="inputName" class="control-label mb-10">Contraseña del usuario</label>
															<input type="password" class="form-control" id="input_usuario_contrasena"  name="usuario_contrasena" placeholder="Introduce la contraseña">
														</div>
														<?php
														if(isset($fallo_usuario_contrasena)){
															echo '<script type="text/javascript">
															document.getElementById("input_usuario_contrasena").oninput=comprobar;
															document.getElementById("input_usuario_contrasena").style.borderColor="'.$fallo_usuario_contrasena.'";
															</script>';
														}
														?>
														<div class="form-group">
															<label for="inputName" class="control-label mb-10">Repita la contraseña</label>
															<input type="password" class="form-control" id="input_usuario_contrasena2"  name="usuario_contrasena2" placeholder="Introduce de nuevo la contraseña">
														</div>
														<?php
														if(isset($fallo_usuario_contrasena)){
															echo '<script type="text/javascript">
															document.getElementById("input_usuario_contrasena2").oninput=comprobar;
															document.getElementById("input_usuario_contrasena2").style.borderColor="'.$fallo_usuario_contrasena.'";
															</script>';
														}
														?>
														<div class="form-group">
															<div class="checkbox checkbox-success">
																<input id="checkbox_politicas" type="checkbox" name="politicas">
																<label for="checkbox_politicas">
																	Acepto las <a href="https://www.casfid.es/es/politica-privacidad.php" target="_blank">políticas de privacidad</a> y <a href="https://www.casfid.es/es/politica-cookies.php" target="_blank">la política de cookies</a>, y he leído el <a href="https://www.casfid.es/es/aviso-legal" target="_blank">aviso legal</a>.
																</label>
															</div>
														</div>
														<div class="form-group">
															<div class="checkbox checkbox-success">
																<input id="enviar_email" type="checkbox" name="enviar_email">
																<label for="enviar_email">
																	Deseo recibir un email de información para saber los datos de la cuenta que he creado.
																</label>
															</div>
														</div>
														<div class="form-group mb-0">
															<button type="submit" class="btn btn-success btn-anim" id="boton_todo" disabled><i class="icon-rocket"></i><span class="btn-text">Registrarse</span></button>
														</div>
													</form>
													<script type="text/javascript">
														document.getElementById('input_usuario_contrasena').oninput=comparar;
														document.getElementById('input_usuario_contrasena2').oninput=comparar;

														document.getElementById('checkbox_politicas').onclick=activar;


														function activar(event){
															var el_input=event.target;

															if(el_input.checked){
																document.getElementById('boton_todo').disabled=false;
															}else{
																document.getElementById('boton_todo').disabled=true;
															}
														}

														function comparar(event){
															var el_input=event.target;

															if(el_input.value==''){
																el_input.style.borderColor="";
															}else if(document.getElementById('input_usuario_contrasena').value!=document.getElementById('input_usuario_contrasena2').value){
																document.getElementById('input_usuario_contrasena').style.borderColor='red';
																document.getElementById('input_usuario_contrasena2').style.borderColor='red';
															}else if(document.getElementById('input_usuario_contrasena').value==document.getElementById('input_usuario_contrasena2').value){
																document.getElementById('input_usuario_contrasena').style.borderColor='green';
																document.getElementById('input_usuario_contrasena2').style.borderColor='green';
															}
														}
													</script>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Footer -->
					<?php
					Tools::crearFooter();
					?>
					<!-- /Footer -->
				</div>
				
				<!-- /Main Content -->

			</div>
			<!-- /#wrapper -->

			<!-- JavaScript -->

			<!-- jQuery -->
			<script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>

			<!-- Bootstrap Core JavaScript -->
			<script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

			<!-- Data table JavaScript -->
			<script src="vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>

			<!-- Slimscroll JavaScript -->
			<script src="dist/js/jquery.slimscroll.js"></script>

			<!-- Progressbar Animation JavaScript -->
			<script src="vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
			<script src="vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>

			<!-- Fancy Dropdown JS -->
			<script src="dist/js/dropdown-bootstrap-extended.js"></script>

			<!-- Sparkline JavaScript -->
			<script src="vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>

			<!-- Owl JavaScript -->
			<script src="vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>

			<!-- Switchery JavaScript -->
			<script src="vendors/bower_components/switchery/dist/switchery.min.js"></script>

			<!-- EChartJS JavaScript -->
			<script src="vendors/bower_components/echarts/dist/echarts-en.min.js"></script>
			<script src="vendors/echarts-liquidfill.min.js"></script>

			<!-- Toast JavaScript -->
			<script src="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>

			<!-- Init JavaScript -->
			<script src="dist/js/init.js"></script>
			<?php
			if(isset($_SESSION['registro_completo'])){
				echo '<script src="dist/js/dashboard-data.js"></script>';

				unset($_SESSION['registro_completo']);
			}
			?>
		</body>
		<?php
		unset($_SESSION['cache_user']);
		?>
		</html>
