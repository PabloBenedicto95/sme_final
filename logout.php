<?php
session_start();

//Borra todas las cookies que hay guardadas de este servidor
foreach ($_COOKIE as $key => $value) {
	unset($_COOKIE[$key]);
}

session_destroy();

header('Location: ./');
?>