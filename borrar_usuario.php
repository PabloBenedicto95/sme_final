<?php
session_start();
include 'modelos/Tools.php';
$enlace=Tools::conexion();
$id_usuario=mysqli_real_escape_string($enlace, filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT));

//Asigno una vuelta rápida a la página eventos
$_SESSION['cache_url']=$_SESSION['ultima_pagina_borrar_usuario'];
unset($_SESSION['ultima_pagina_borrar_usuario']);
Tools::paginaAnterior();

if(!isset($_SESSION['admin'])){
	$consulta=mysqli_query($enlace, 'SELECT * FROM sme_usuarios WHERE ID='.$id_usuario);
	if(mysqli_fetch_assoc($consulta)['id_cliente']!=$_SESSION['id_cliente']){
		header('Location: ./');
	}
}

if(mysqli_query($enlace, 'DELETE FROM sme_usuarios WHERE ID='.$id_usuario)){
	$_SESSION['registro_completo']=true;
}

header('Location: ./');
?>