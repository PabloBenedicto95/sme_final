<?php
session_start();

if(isset($_SESSION['registro_rapido'])){
	$envio=$_SESSION['registro_rapido'];
	unset($_SESSION['registro_rapido']);
	header('Location: '.$envio);
}else{
	if(isset($_SESSION['usuario'])){
		header('Location: inicio.php');
	}else{
		header('Location: login.php');
	}
}
?>