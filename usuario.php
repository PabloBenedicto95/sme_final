<!DOCTYPE html>
<html lang="en">
<head>
	<?php
	session_start();

	if(!isset($_GET['id'])){
		header('Location: ./');
	}

	include_once('modelos/Tools.php');
	Tools::comprobarMismoCliente($_GET['id']);

	$enlace=Tools::crearHeaderYConexion();

	$numero=mysqli_real_escape_string($enlace, filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT));
	$consulta=mysqli_query($enlace, 'select * from sme_usuarios where ID='.$numero);

	$_SESSION['cache_user']=mysqli_fetch_assoc($consulta);

	if(!isset($_SESSION['cache_user'])){
		header('Location: ./');
	}

	if(isset($_COOKIE['error_log'])){
		$error_log=1;
		setcookie("error_log","",(-1),"/");
	}

	echo '<title>'.$_SESSION['cache_user']['nombre'].' I Usuario</title>';
	?>
	<!--alerts CSS -->
	<link href="vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
</head>

<body>
	<!--Preloader-->
	<?php
	Tools::preloader();
	?>
	<!--/Preloader-->
	<div class="wrapper theme-1-active pimary-color-blue">
		
		<!-- Top Menu Items -->
		<?php
		Tools::crearMenuTop();
		?>
		<!-- /Top Menu Items -->
		
		<!-- Left Sidebar Menu -->
		<?php
		Tools::crearMenuLeft();
		?>
		<!-- /Left Sidebar Menu -->

		<!-- Main Content -->
		<div class="page-wrapper">
			<div class="container-fluid pt-25">

				<!-- Row -->
				<div class="row">
					<div class="col-lg-3 col-xs-12">
						<div class="panel panel-default card-view  pa-0">
							<div class="panel-wrapper collapse in">
								<div class="panel-body  pa-0">
									<div class="profile-box">
										<div class="profile-cover-pic" style="background-image: url('dist/img/detras.png'); background-size: 100%;">
											<!--<img src="dist/img/detras.png" style="width: 100%;">-->
											<div class="profile-image-overlay" style="background-color: black">
											</div>
										</div>
										<div class="profile-info text-center">
											<div class="profile-img-wrap">
												<img class="inline-block mb-10" src="dist/img/desconocido.jpg" alt="user"/>
											</div>	
											<h5 class="block mt-10 mb-5 weight-500 capitalize-font txt-info" style="color: #00469d !important;"><?php
											echo $_SESSION['cache_user']['nombre'];
											?></h5>
											<h6 class="block capitalize-font pb-20">Usuario interno</h6>
										</div>	
										<div class="social-info">
											<button class="btn btn-info btn-block  btn-anim mt-30" data-toggle="modal" data-target="#myModal" id="boton_editar"><i class="fa fa-pencil"></i><span class="btn-text">Editar usuario</span></button>
											<div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
															<h5 class="modal-title" id="myModalLabel">Editar usuario</h5>
														</div>
														<div class="modal-body">
															<!-- Row -->
															<div class="row">
																<div class="col-lg-12">
																	<div class="">
																		<div class="panel-wrapper collapse in">
																			<div class="panel-body pa-0">
																				<div class="col-sm-12 col-xs-12">
																					<div class="form-wrap">
																						<form action="cambiar_datos_usuario.php" method="post" id="datos_nuevos">
																							<div class="form-body overflow-hide">
																								<div class="form-group">
																									<label class="control-label mb-10" for="exampleInputuname_1">Nombre</label>
																									<div class="input-group">
																										<div class="input-group-addon"><i class="icon-user"></i></div>
																										<input type="text" class="form-control" id="exampleInputuname_1" value="<?php
																										echo $_SESSION['cache_user']['nombre'];
																										?>" name="nombre" required>
																									</div>
																								</div>
																								<?php

																								echo '<div class="form-group">
																								<label class="control-label mb-10" for="exampleInputEmail_1">Nombre de usuario</label>
																								<div class="input-group">
																								<div class="input-group-addon"><i class="icon-emotsmile"></i></div>
																								<input type="text" class="form-control" id="exampleInputEmail_1" ';
																								if($_SESSION['cache_user']['usuario']!=''){
																									echo 'value="'.$_SESSION['cache_user']['usuario'].'"';
																								}
																								echo ' name="usuario" required>
																								</div>
																								</div>';

																								echo '<div class="form-group">
																								<label class="control-label mb-10" for="exampleInputEmail_1">Contacto de email</label>
																								<div class="input-group">
																								<div class="input-group-addon"><i class="icon-envelope-open"></i></div>
																								<input type="email" class="form-control" id="exampleInputEmail_1" ';
																								if($_SESSION['cache_user']['email']!=''){
																									echo 'value="'.$_SESSION['cache_user']['email'].'"';
																								}
																								echo ' name="email">
																								</div>
																								</div>

																								<div class="form-group">
																								<label class="control-label mb-10" for="exampleInputEmail_1">DNI</label>
																								<div class="input-group">
																								<div class="input-group-addon"><i class="icon-wallet"></i></div>
																								<input type="text" class="form-control" id="exampleInputEmail_1" ';
																								if($_SESSION['cache_user']['dni']!=''){
																									echo 'value="'.$_SESSION['cache_user']['dni'].'"';
																								}
																								echo ' name="dni">
																								</div>
																								</div>

																								<div class="form-group">
																								<label class="control-label mb-10" for="exampleInputContact_1">Número de teléfono</label>
																								<div class="input-group">
																								<div class="input-group-addon"><i class="icon-phone"></i></div>
																								<input type="text" data-mask="(999) 999-999" class="form-control" id="telefono" name="telefono"';
																								if($_SESSION['cache_user']['telefono']!=''){
																									echo 'value="'.$_SESSION['cache_user']['telefono'].'"';
																								}


																								echo ' name="telefono">
																								</div>
																								</div>

																								<script type="text/javascript">
																								document.getElementById(\'telefono\').oninput=cambioNumero;

																								function cambioNumero(evento){
																									var input=evento.target;

																									input.value=parseInt(input.value);
																									if(input.value==\'NaN\'){
																										input.value=\'\';
																									}

																									var array=input.value.split(\'\');

																									if(array.length==9){
																										var mostrar=\'\';
																										for(var i=0; i<3; i++){
																											mostrar+=array[i];
																										}
																										mostrar+=\'\';

																										for(var i=3; i<9; i++){
																											mostrar+=array[i];
																											if(i==5){
																												mostrar+=\'\';
																											}
																										}
																										input.value=mostrar;
																										}else if(array.length>9){
																											var mostrar=\'\';
																											for(var i=0; i<9; i++){
																												mostrar+=array[i];
																											}
																											input.value=mostrar;
																											cambioNumero(evento);
																										}
																									}
																									</script>
																									<div class="form-group">
																									<label class="control-label mb-10" for="exampleInputpwd_1">Contraseña</label>
																									<div class="input-group">
																									<div class="input-group-addon"><i class="icon-lock"></i></div>
																									<input type="password" class="form-control" id="exampleInputpwd_1" value="'.$_SESSION['cache_user']['contrasena'].'" required name="contrasena" required>
																									</div>
																									</div>';
																									?>
																								</div>	
																							</form>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="modal-footer">
																<script type="text/javascript">
																	document.cookie='retorno='+window.location.href;
																	document.cookie='cache_user=<?php
																	echo Tools::encriptar(json_encode($_SESSION['cache_user'], TRUE));
																	?>';
																</script>
																<button class="btn btn-success waves-effect" data-dismiss="modal" alt="alert" id="sa-params-usuario">Guardar cambios</button>
																<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancelar</button>
															</div>
														</div>
														<!-- /.modal-content -->
													</div>
													<!-- /.modal-dialog -->
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-9 col-xs-12">
							<div class="panel panel-default card-view pa-0">
								<div class="panel-wrapper collapse in">
									<div  class="panel-body pb-0">
										<div  class="tab-struct custom-tab-1">
											<ul role="tablist" class="nav nav-tabs nav-tabs-responsive" id="myTabs_8">
												<li class="active" role="presentation"><a  data-toggle="tab" id="profile_tab_8" role="tab" href="#profile_8" aria-expanded="false"><span>Datos</span></a></li>
											</ul>
											<div class="tab-content" id="myTabContent_8">
												<div class="row">
													<div class="col-lg-12">
														<div class="">
															<div class="panel-wrapper collapse in">
																<div class="panel-body pa-0">
																	<div class="col-sm-12 col-xs-12">
																		<div class="form-wrap">
																			<form action="#">
																				<div class="form-body overflow-hide">
																					<div class="form-group">
																						<label class="control-label mb-10" for="exampleInputuname_1">Nombre</label>
																						<div class="input-group">
																							<div class="input-group-addon"><i class="icon-user"></i></div>
																							<input type="text" class="form-control" id="exampleInputuname_1" value="<?php
																							echo $_SESSION['cache_user']['nombre'];
																							?>" readonly style="background-color: ">
																						</div>
																					</div>
																					<?php
																					if($_SESSION['cache_user']['usuario']!=''){
																						echo '<div class="form-group">
																						<label class="control-label mb-10" for="exampleInputEmail_1">Nombre de usuario</label>
																						<div class="input-group">
																						<div class="input-group-addon"><i class="icon-emotsmile"></i></div>
																						<input type="text" class="form-control" id="exampleInputEmail_1" value="'.$_SESSION['cache_user']['usuario'].'" required readonly style="background-color: ">
																						</div>
																						</div>';
																					}

																					if($_SESSION['cache_user']['email']!=''){
																						echo '<div class="form-group">
																						<label class="control-label mb-10" for="exampleInputEmail_1">Contacto de email</label>
																						<div class="input-group">
																						<div class="input-group-addon"><i class="icon-envelope-open"></i></div>
																						<input type="email" class="form-control" id="exampleInputEmail_1" value="'.$_SESSION['cache_user']['email'].'" required readonly style="background-color: ">
																						</div>
																						</div>';
																					}

																					if($_SESSION['cache_user']['dni']!=''){
																						echo '<div class="form-group">
																						<label class="control-label mb-10" for="exampleInputEmail_1">DNI</label>
																						<div class="input-group">
																						<div class="input-group-addon"><i class="icon-wallet"></i></div>
																						<input type="text" class="form-control" id="exampleInputEmail_1" value="'.$_SESSION['cache_user']['dni'].'" required readonly style="background-color: ">
																						</div>
																						</div>';
																					}

																					if($_SESSION['cache_user']['telefono']!=''){
																						echo '<div class="form-group">
																						<label class="control-label mb-10" for="exampleInputContact_1">Número de teléfono</label>
																						<div class="input-group">
																						<div class="input-group-addon"><i class="icon-phone"></i></div>
																						<input type="text" data-mask="(999) 999-999" class="form-control" id="telefono" name="telefono" value="'.$_SESSION['cache_user']['telefono'].'" required readonly style="background-color: ">
																						</div>
																						</div>

																						<script type="text/javascript">
																						document.getElementById(\'telefono\').oninput=cambioNumero;

																						function cambioNumero(evento){
																							var input=evento.target;

																							input.value=parseInt(input.value);
																							if(input.value==\'NaN\'){
																								input.value=\'\';
																							}

																							var array=input.value.split(\'\');

																							if(array.length==9){
																								var mostrar=\'\';
																								for(var i=0; i<3; i++){
																									mostrar+=array[i];
																								}
																								mostrar+=\'\';

																								for(var i=3; i<9; i++){
																									mostrar+=array[i];
																									if(i==5){
																										mostrar+=\'\';
																									}
																								}
																								input.value=mostrar;
																								}else if(array.length>9){
																									var mostrar=\'\';
																									for(var i=0; i<9; i++){
																										mostrar+=array[i];
																									}
																									input.value=mostrar;
																									cambioNumero(evento);
																								}
																							}
																							</script>';
																						}

																						if($_SESSION['cache_user']['contrasena']){
																							echo '<div class="form-group">
																							<label class="control-label mb-10" for="exampleInputpwd_1">Contraseña</label>
																							<div class="input-group">
																							<div class="input-group-addon"><i class="icon-lock"></i></div>
																							<input type="password" class="form-control" id="exampleInputpwd_1" value="'.$_SESSION['cache_user']['contrasena'].'" required readonly style="background-color: ">
																							</div>
																							</div>';
																						}
																						?>
																					</div>			
																				</form>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>


						</div>
					</div>
					<!-- /Row -->
					<!-- Footer -->
					<?php
					Tools::crearFooter();
					?>
					<!-- /Footer -->
				</div>
			</div>
			<!-- /Main Content -->

		</div>
		<!-- /#wrapper -->

		<!-- JavaScript -->

		<!-- jQuery -->
		<script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>

		<!-- Bootstrap Core JavaScript -->
		<script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<script src="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>

		<!-- Vector Maps JavaScript -->
		<script src="vendors/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
		<script src="vendors/vectormap/jquery-jvectormap-world-mill-en.js"></script>
		<script src="dist/js/vectormap-data.js"></script>

		<!-- Calender JavaScripts -->
		<script src="vendors/bower_components/moment/min/moment.min.js"></script>
		<script src="vendors/jquery-ui.min.js"></script>
		<script src="vendors/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
		<script src="dist/js/fullcalendar-data.js"></script>

		<!-- Counter Animation JavaScript -->
		<script src="vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
		<script src="vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>

		<!-- Data table JavaScript -->
		<script src="vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>

		<!-- Slimscroll JavaScript -->
		<script src="dist/js/jquery.slimscroll.js"></script>

		<!-- Fancy Dropdown JS -->
		<script src="dist/js/dropdown-bootstrap-extended.js"></script>

		<!-- Sparkline JavaScript -->
		<script src="vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>

		<script src="vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
		<script src="dist/js/skills-counter-data.js"></script>

		<!-- Morris Charts JavaScript -->
		<script src="vendors/bower_components/raphael/raphael.min.js"></script>
		<script src="vendors/bower_components/morris.js/morris.min.js"></script>
		<script src="dist/js/morris-data.js"></script>

		<!-- Owl JavaScript -->
		<script src="vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>

		<!-- Switchery JavaScript -->
		<script src="vendors/bower_components/switchery/dist/switchery.min.js"></script>

		<!-- Data table JavaScript -->
		<script src="vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>

		<!-- Gallery JavaScript -->
		<script src="dist/js/isotope.js"></script>
		<script src="dist/js/lightgallery-all.js"></script>
		<script src="dist/js/froogaloop2.min.js"></script>
		<script src="dist/js/gallery-data.js"></script>

		<!-- Sweet-Alert  -->
		<script src="vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>	
		<script src="dist/js/sweetalert-data.js"></script>

		<!-- twitter JavaScript -->
		<script src="dist/js/twitterFetcher.js"></script>

		<!-- Spectragram JavaScript -->
		<script src="dist/js/spectragram.min.js"></script>

		<!-- Init JavaScript -->
		<script src="dist/js/init.js"></script>
		<?php
		if(isset($error_log)){
			echo '<script src="dist/js/toast-data.js"></script>';
			unset($error_log);
		}
		?>

	</body>
	<?php
	unset($_SESSION['cache_user']);
	?>
	</html>
