<?php
session_start();
if(!isset($_SESSION['usuario'])){
	header('Location: ./');
}

if(isset($_COOKIE['retorno']) and isset($_COOKIE['numero'])){
	$_SESSION['registro_rapido']=$_COOKIE['retorno'];
	$numero=$_COOKIE['numero'];
	setcookie('retorno', '', -1, '/');
	setcookie('numero', '', -1, '/');
}

$todoOK=true;

include('modelos/Tools.php');
$enlace = Tools::conexion();

$numero=mysqli_real_escape_string($enlace, filter_var($numero, FILTER_SANITIZE_NUMBER_INT));

$datos=array();

if(isset($_POST['nombre'])){
	if(filter_var($_POST['nombre'], FILTER_SANITIZE_STRING)!=''){
		$datos['nombre']=mysqli_real_escape_string($enlace, filter_var($_POST['nombre'], FILTER_SANITIZE_STRING));
	}else{
		setcookie("fallo_nombre","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}

if(isset($_POST['nombre_sms']) and $_POST['nombre_sms']!=''){
	if(filter_var($_POST['nombre_sms'], FILTER_SANITIZE_STRING)!=''){
		$datos['nombre_sms']=mysqli_real_escape_string($enlace, filter_var($_POST['nombre_sms'], FILTER_SANITIZE_STRING));
	}else{
		$datos['nombre_sms']='';
	}
}else{
	$datos['nombre_sms']='';
}

if(isset($_POST['imagen']) and $_POST['imagen']!=''){
	if(filter_var($_POST['imagen'], FILTER_SANITIZE_URL)!=''){
		$datos['imagen']=mysqli_real_escape_string($enlace, filter_var($_POST['imagen'], FILTER_SANITIZE_URL));
	}else{
		$datos['imagen']='';
	}
}else{
	$datos['imagen']='';
}

if(isset($_POST['lugar']) and $_POST['lugar']!=''){
	if(filter_var($_POST['lugar'], FILTER_SANITIZE_STRING)!=''){
		$datos['lugar']=mysqli_real_escape_string($enlace, filter_var($_POST['lugar'], FILTER_SANITIZE_STRING));
	}else{
		$datos['lugar']='';
	}
}else{
	$datos['lugar']='';
}

if(isset($_POST['fecha']) and $_POST['fecha']!=''){
	if(filter_var($_POST['fecha'], FILTER_SANITIZE_STRING)!=''){
		$datos['fecha']=mysqli_real_escape_string($enlace, filter_var($_POST['fecha'], FILTER_SANITIZE_STRING));
	}else{
		$datos['fecha']='';
	}
}else{
	$datos['fecha']='';
}

if(isset($_POST['mostrar_comprobar_pulsera'])){
	if(filter_var($_POST['mostrar_comprobar_pulsera'], FILTER_SANITIZE_EMAIL)!=''){
		$datos['mostrar_comprobar_pulsera']=mysqli_real_escape_string($enlace, filter_var($_POST['mostrar_comprobar_pulsera'], FILTER_SANITIZE_EMAIL));
		if($datos['mostrar_comprobar_pulsera']=='on'){
			$datos['mostrar_comprobar_pulsera']=1;
		}else{
			$datos['mostrar_comprobar_pulsera']=0;
		}
	}else{
		$datos['mostrar_comprobar_pulsera']=0;
	}
}else{
	$datos['mostrar_comprobar_pulsera']=0;
}

if(isset($_POST['registro_previo'])){
	if(filter_var($_POST['registro_previo'], FILTER_SANITIZE_EMAIL)!=''){
		$datos['registro_previo']=mysqli_real_escape_string($enlace, filter_var($_POST['registro_previo'], FILTER_SANITIZE_EMAIL));
		if($datos['registro_previo']=='on'){
			$datos['registro_previo']=1;
		}else{
			$datos['registro_previo']=0;
		}
	}else{
		$datos['registro_previo']=0;
	}
}else{
	$datos['registro_previo']=0;
}

if(isset($_POST['registro_email'])){
	if(filter_var($_POST['registro_email'], FILTER_SANITIZE_EMAIL)!=''){
		$datos['registro_email']=mysqli_real_escape_string($enlace, filter_var($_POST['registro_email'], FILTER_SANITIZE_EMAIL));
		if($datos['registro_email']=='on'){
			$datos['registro_email']=1;
		}else{
			$datos['registro_email']=0;
		}
	}else{
		$datos['registro_email']=0;
	}
}else{
	$datos['registro_email']=0;
}

if(isset($_POST['registro_telefono'])){
	if(filter_var($_POST['registro_telefono'], FILTER_SANITIZE_EMAIL)!=''){
		$datos['registro_telefono']=mysqli_real_escape_string($enlace, filter_var($_POST['registro_telefono'], FILTER_SANITIZE_EMAIL));
		if($datos['registro_telefono']=='on'){
			$datos['registro_telefono']=1;
		}else{
			$datos['registro_telefono']=0;
		}
	}else{
		$datos['registro_telefono']=0;
	}
}else{
	$datos['registro_telefono']=0;
}

$datos['id_cliente']=$_SESSION['id_cliente'];

if(isset($_POST['id_lugar_fb']) and $_POST['id_lugar_fb']!=''){
	if(filter_var($_POST['id_lugar_fb'], FILTER_SANITIZE_NUMBER_INT)){
		$datos['id_lugar_fb']=mysqli_real_escape_string($enlace, filter_var($_POST['id_lugar_fb'], FILTER_SANITIZE_NUMBER_INT));
	}else{
		$datos['id_lugar_fb']='';
	}
}else{
	$datos['id_lugar_fb']='';
}

if(isset($_POST['facebook_pagina']) and $_POST['facebook_pagina']!=''){
	if(filter_var($_POST['facebook_pagina'], FILTER_SANITIZE_EMAIL)){
		$datos['facebook_pagina']=mysqli_real_escape_string($enlace, filter_var($_POST['facebook_pagina'], FILTER_SANITIZE_EMAIL));
	}else{
		$datos['facebook_pagina']='';
	}
}else{
	$datos['facebook_pagina']='';
}

if(isset($_POST['url']) and $_POST['url']!=''){
	if(filter_var($_POST['url'], FILTER_SANITIZE_URL)){
		$datos['url']=mysqli_real_escape_string($enlace, filter_var($_POST['url'], FILTER_SANITIZE_URL));
	}else{
		$datos['url']='';
	}
}else{
	$datos['url']='';
}

if($todoOK){
	$consulta='UPDATE sme_eventos SET ';

	$primero=true;
	foreach ($datos as $x) {
		if(!$primero){
			$consulta.=', ';
		}else{
			$primero=false;
		}

		if(is_int($x)){
			$consulta.=key($datos).'='.$x;
		}else{
			$consulta.=key($datos).'="'.$x.'"';
		}

		next($datos);
	}
	
	$consulta.=' WHERE ID='.$numero;

	$accion=mysqli_query($enlace, $consulta);

	if($accion){
		$_SESSION['registro_completo']='El evento '.$_POST['nombre'].' ha sido creado con éxito';
	}

	header('Location: ./');
}else{
	header('Location: ./');
}
?>