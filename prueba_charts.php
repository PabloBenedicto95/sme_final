<!DOCTYPE html>
<html>
<head>
	<title>Prueba charts</title>
	<meta charset="utf-8">
</head>
<body>
	<script type="text/javascript" src="./vendors/chart.js/Chart.min.js"></script>

	<div style="width: 50%; height: 35%;">
		<canvas id="myChart"></canvas>
	</div>

	<script type="text/javascript">
		var ctx = document.getElementById("myChart").getContext('2d');

		var mixedChart = new Chart(ctx, {
			type: 'bar',
			data: {
				datasets: [{
					label: 'Bar Dataset',
					data: [<?php
						echo $video;
						?>, <?php
						echo $registro_en_accion;
						?>, <?php
						echo $texto_personalizado;
						?>, <?php
						echo $sin_foto;
						?>, <?php
						echo $etiquetado_multiple;
						?>],
						backgroundColor: [
						'rgba(255, 99, 132, 1)',
						'rgba(54, 162, 235, 1)',
						'rgba(255, 206, 86, 1)',
						'rgba(75, 192, 192, 1)',
						'rgba(153, 102, 255, 1)'
						],
						borderColor: [
						'rgba(255,99,132,0.1)',
						'rgba(54, 162, 235, 0.1)',
						'rgba(255, 206, 86, 0.1)',
						'rgba(75, 192, 192, 0.1)',
						'rgba(153, 102, 255, 0.1)'
						],
						borderWidth: 1
					}, {
						label: 'Line Dataset',
						data: [<?php
							echo $contador;
							?>],

          // Changes this dataset to become a line
          type: 'line'
      }],
      labels: ['January', 'February', 'March', 'April']
  }
});
</script>
</body>
</html>