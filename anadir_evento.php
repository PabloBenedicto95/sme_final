<?php
session_start();
if(!isset($_SESSION['usuario']) or !isset($_SESSION['admin'])){
	header('Location: ./');
}

$todoOK=true;

include('modelos/Tools.php');
$enlace = Tools::conexion();

$datos=array();

if(isset($_POST['nombre'])){
	if(filter_var($_POST['nombre'], FILTER_SANITIZE_STRING)!=''){
		$datos['nombre']=mysqli_real_escape_string($enlace, filter_var($_POST['nombre'], FILTER_SANITIZE_STRING));
	}else{
		setcookie("fallo_nombre","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}

if(isset($_POST['nombre_sms']) and $_POST['nombre_sms']!=''){
	if(filter_var($_POST['nombre_sms'], FILTER_SANITIZE_STRING)!=''){
		$datos['nombre_sms']=mysqli_real_escape_string($enlace, filter_var($_POST['nombre_sms'], FILTER_SANITIZE_STRING));
	}else{
		setcookie("fallo_nombre_sms","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}

if(isset($_POST['imagen']) and $_POST['imagen']!=''){
	if(filter_var($_POST['imagen'], FILTER_SANITIZE_URL)!=''){
		$datos['imagen']=mysqli_real_escape_string($enlace, filter_var($_POST['imagen'], FILTER_SANITIZE_URL));
	}else{
		setcookie("fallo_imagen","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}

if(isset($_POST['lugar']) and $_POST['lugar']!=''){
	if(filter_var($_POST['lugar'], FILTER_SANITIZE_STRING)!=''){
		$datos['lugar']=mysqli_real_escape_string($enlace, filter_var($_POST['lugar'], FILTER_SANITIZE_STRING));
	}else{
		setcookie("fallo_lugar","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}

if(isset($_POST['fecha']) and $_POST['fecha']!=''){
	if(filter_var($_POST['fecha'], FILTER_SANITIZE_STRING)!=''){
		$datos['fecha']=mysqli_real_escape_string($enlace, filter_var($_POST['fecha'], FILTER_SANITIZE_STRING));
	}else{
		setcookie("fallo_fecha","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}

if(isset($_POST['mostrar_comprobar_pulsera'])){
	if(filter_var($_POST['mostrar_comprobar_pulsera'], FILTER_SANITIZE_EMAIL)!=''){
		$datos['mostrar_comprobar_pulsera']=mysqli_real_escape_string($enlace, filter_var($_POST['mostrar_comprobar_pulsera'], FILTER_SANITIZE_EMAIL));
		if($datos['mostrar_comprobar_pulsera']=='on'){
			$datos['mostrar_comprobar_pulsera']=1;
		}else{
			$datos['mostrar_comprobar_pulsera']=0;
		}
	}else{
		setcookie("fallo_mostrar_comprobar_pulsera","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}

if(isset($_POST['registro_previo'])){
	if(filter_var($_POST['registro_previo'], FILTER_SANITIZE_EMAIL)!=''){
		$datos['registro_previo']=mysqli_real_escape_string($enlace, filter_var($_POST['registro_previo'], FILTER_SANITIZE_EMAIL));
		if($datos['registro_previo']=='on'){
			$datos['registro_previo']=1;
		}else{
			$datos['registro_previo']=0;
		}
	}else{
		setcookie("fallo_registro_previo","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}

if(isset($_POST['registro_email'])){
	if(filter_var($_POST['registro_email'], FILTER_SANITIZE_EMAIL)!=''){
		$datos['registro_email']=mysqli_real_escape_string($enlace, filter_var($_POST['registro_email'], FILTER_SANITIZE_EMAIL));
		if($datos['registro_email']=='on'){
			$datos['registro_email']=1;
		}else{
			$datos['registro_email']=0;
		}
	}else{
		setcookie("fallo_registro_email","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}

if(isset($_POST['registro_telefono'])){
	if(filter_var($_POST['registro_telefono'], FILTER_SANITIZE_EMAIL)!=''){
		$datos['registro_telefono']=mysqli_real_escape_string($enlace, filter_var($_POST['registro_telefono'], FILTER_SANITIZE_EMAIL));
		if($datos['registro_telefono']=='on'){
			$datos['registro_telefono']=1;
		}else{
			$datos['registro_telefono']=0;
		}
	}else{
		setcookie("fallo_registro_telefono","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}

if(isset($_POST['id_cliente']) and $_POST['id_cliente']!=''){
	if(filter_var($_POST['id_cliente'], FILTER_SANITIZE_NUMBER_INT)){
		$datos['id_cliente']=mysqli_real_escape_string($enlace, filter_var($_POST['id_cliente'], FILTER_SANITIZE_NUMBER_INT));
	}else{
		setcookie("fallo_id_cliente","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}

if(isset($_POST['id_lugar_fb']) and $_POST['id_lugar_fb']!=''){
	if(filter_var($_POST['id_lugar_fb'], FILTER_SANITIZE_NUMBER_INT)){
		$datos['id_lugar_fb']=mysqli_real_escape_string($enlace, filter_var($_POST['id_lugar_fb'], FILTER_SANITIZE_NUMBER_INT));
	}else{
		setcookie("fallo_id_lugar_fb","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}

if(isset($_POST['facebook_pagina']) and $_POST['facebook_pagina']!=''){
	if(filter_var($_POST['facebook_pagina'], FILTER_SANITIZE_EMAIL)){
		$datos['facebook_pagina']=mysqli_real_escape_string($enlace, filter_var($_POST['facebook_pagina'], FILTER_SANITIZE_EMAIL));
	}else{
		setcookie("fallo_facebook_pagina","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}

if(isset($_POST['url']) and $_POST['url']!=''){
	if(filter_var($_POST['url'], FILTER_SANITIZE_URL)){
		$datos['url']=mysqli_real_escape_string($enlace, filter_var($_POST['url'], FILTER_SANITIZE_URL));
	}else{
		setcookie("fallo_url","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}

if($todoOK){
	$consulta='insert into sme_eventos (';

	$primero=true;
	foreach ($datos as $x) {
		if(!$primero){
			$consulta.=', ';
		}else{
			$primero=false;
		}

		$consulta.=key($datos);

		next($datos);
	}
	
	$consulta.=') values (';

	$primero=true;
	foreach ($datos as $x) {
		if(!$primero){
			$consulta.=', ';
		}else{
			$primero=false;
		}

		if(is_string($x)){
			$consulta.='\''.$x.'\'';
		}else{
			$consulta.=$x;
		}
	}

	$consulta.=')';

	$accion=mysqli_query($enlace, $consulta);

	if($accion){
		$_SESSION['registro_completo']='El usuario '.$_POST['nombre'].' ha sido creado con éxito';
	}

	header('Location: nuevo_evento.php');
}else{
	header('Location: nuevo_evento.php');
}
?>