<?php
session_start();

$todoOK=true;

include('modelos/Tools.php');
$enlace = Tools::conexion();

if(isset($_POST['email']) and $_POST['email']!=''){
	if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)!=''){
		$email=mysqli_real_escape_string($enlace, filter_var($_POST['email'], FILTER_SANITIZE_EMAIL));
	}else{
		setcookie("fallo_email","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}else{
	setcookie("fallo_email","red",time()+60*60*24*365,"/");
	$todoOK=false;
}

if(isset($_POST['nombre'])){
	if(filter_var($_POST['nombre'], FILTER_SANITIZE_EMAIL)!=''){
		$nombre=mysqli_real_escape_string($enlace, filter_var($_POST['nombre'], FILTER_SANITIZE_EMAIL));
	}else{
		setcookie("fallo_nombre","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}else{
	setcookie("fallo_nombre","red",time()+60*60*24*365,"/");
	$todoOK=false;
}

if(isset($_POST['usuario'])){
	if(filter_var($_POST['usuario'], FILTER_SANITIZE_EMAIL)!=''){
		$usuario=mysqli_real_escape_string($enlace, filter_var($_POST['usuario'], FILTER_SANITIZE_EMAIL));
	}else{
		setcookie("fallo_usuario","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}else{
	setcookie("fallo_usuario","red",time()+60*60*24*365,"/");
	$todoOK=false;
}

if(isset($_POST['usuario_contrasena']) and $_POST['usuario_contrasena']==$_POST['usuario_contrasena2']){
	if(filter_var($_POST['usuario_contrasena'], FILTER_SANITIZE_EMAIL)!=''){
		$usuario_contrasena=mysqli_real_escape_string($enlace, filter_var($_POST['usuario_contrasena'], FILTER_SANITIZE_EMAIL));
	}else{
		setcookie("fallo_usuario_contrasena","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}else{
	setcookie("fallo_usuario_contrasena","red",time()+60*60*24*365,"/");
	$todoOK=false;
}

if(isset($_POST['telefono']) and $_POST['telefono']!=''){
	if(filter_var($_POST['telefono'], FILTER_VALIDATE_INT)){
		$telefono=mysqli_real_escape_string($enlace, filter_var($_POST['telefono'], FILTER_SANITIZE_NUMBER_INT));
	}else{
		setcookie("fallo_telefono","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}

$consulta_cache=mysqli_query($enlace, 'SELECT * FROM sme_clientes WHERE nombre="'.$nombre.'"');
if(mysqli_num_rows($consulta_cache)!=0){
	setcookie("fallo_nombre","red",time()+60*60*24*365,"/");
	$todoOK=false;
}

$consulta_cache=mysqli_query($enlace, 'SELECT * FROM sme_usuarios WHERE usuario="'.$nombre.'"');
if(mysqli_num_rows($consulta_cache)!=0){
	setcookie("fallo_usuario","red",time()+60*60*24*365,"/");
	$todoOK=false;
}

if(isset($_POST['enviar_email']) and $email==''){
	setcookie("fallo_email","red",time()+60*60*24*365,"/");
	$todoOK=false;
}

if($todoOK and isset($_POST['politicas'])){
	$consulta='insert into sme_clientes (nombre';

	if(isset($email)){
		$consulta.=', email';
	}

	if(isset($telefono)){
		$consulta.=', telefono';
	}

	$consulta.=') values ("'.$nombre.'"';

	if(isset($email)){
		$consulta.=', "'.$email.'"';
		unset($email);
	}

	if(isset($telefono)){
		$consulta.=', "'.$telefono.'"';
		unset($telefono);
	}

	$consulta.=')';

	$accion=mysqli_query($enlace, $consulta);

	$id_cliente=mysqli_fetch_assoc(mysqli_query($enlace, 'SELECT * FROM sme_clientes WHERE nombre="'.$nombre.'"'))['ID'];

	$string='INSERT INTO sme_usuarios (nombre, usuario, contrasena, id_cliente, email) VALUES ("'.$nombre.'", "'.$usuario.'", "'.$usuario_contrasena.'", '.$id_cliente.', "'.$email.'")';

	$accion2=mysqli_query($enlace, $string);

	if(isset($_POST['enviar_email'])){
		$para      = $email;
		$titulo    = 'Registro completo SME';
		$mensaje   = 'Ha sido usted registrado como cliente de CASFID en la palicación SME. Ahora podrá crear y visualizar eventos, crear usuarios para que los gestionen y modificar datos, todo desde una sola aplicación. El usuario creado para entrar es '.$usuario.'';
		$cabeceras = 'From: sme@casfid.es' . "\r\n" .
					'Reply-To: sme@casfid.es' . "\r\n" .
		    		'X-Mailer: PHP/' . phpversion();

		mail($para, $titulo, $mensaje, $cabeceras);
	}

	if(($accion and $accion2) and !isset($_SESSION['admin'])){
		$_SESSION['registro_completo']='El cliente '.$_POST['nombre'].' ha sido creado con éxito';

		$_SESSION['usuario']=$usuario;
		$_SESSION['contrasena']=$usuario_contrasena;

		$consulta=mysqli_query($enlace, 'select * from sme_usuarios where usuario="'.$usuario.'"');
		while($fila=mysqli_fetch_array($consulta)){
			$_SESSION['id']=$fila['ID'];
			$_SESSION['id_cliente']=$fila['id_cliente'];
			if($fila['superadmin']==1){
				$_SESSION['admin']=1;
			}
		}
	}else if(($accion and $accion2) and isset($_SESSION['admin'])){
		$_SESSION['registro_completo']='El cliente '.$_POST['nombre'].' ha sido creado con éxito';
		Tools::paginaAnterior();
	}

}else{
	Tools::paginaAnterior();
}
header('Location: ./');
?>