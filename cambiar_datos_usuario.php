<?php
session_start();
include('modelos/Tools.php');

if(!isset($_POST['nombre']) || !isset($_POST['usuario']) || $_POST['nombre']=='' || $_POST['usuario']=='' || !isset($_POST['email']) || $_POST['email']==''){
	setcookie('error_log', '1', time()+60*60*24*365, '/');
	if(isset($_COOKIE['retorno'])){
		$retorno=$_COOKIE['retorno'];
		setcookie('retorno', '', -1, '/');
		header('Location: '.$retorno);
	}else{
		header('Location: ./');
	}
}else{

	$cache_user=json_decode(Tools::desencriptar($_COOKIE['cache_user']));
	setcookie('cache_user', '', -1, '/');

	$retorno=$_COOKIE['retorno'];
	setcookie('retorno', '', -1, '/');

	Tools::comprobarMismoCliente($cache_user->ID);
	
	$array=array('nombre'=>filter_input(INPUT_POST, 'nombre',FILTER_SANITIZE_EMAIL), 'usuario'=>filter_input(INPUT_POST, 'usuario',FILTER_SANITIZE_EMAIL), 'contrasena'=>filter_input(INPUT_POST, 'contrasena',FILTER_SANITIZE_EMAIL), 'dni'=>filter_input(INPUT_POST, 'dni',FILTER_SANITIZE_EMAIL), 'email'=>filter_input(INPUT_POST, 'email',FILTER_SANITIZE_EMAIL), 'telefono'=>filter_input(INPUT_POST, 'telefono',FILTER_SANITIZE_NUMBER_INT));

	$enlace=Tools::conexion();

	$mapa=array_keys(get_object_vars($cache_user));
	$llaves=array_keys($array);

	foreach($mapa as $elemento){
		$esta=false;
		for($i=0; $i<count($llaves); $i++){
			if($llaves[$i]==$elemento){
				$esta=true;
				break;
			}
		}

		if($esta){
			mysqli_query($enlace, 'UPDATE sme_usuarios SET '.$elemento.'=\''.$array[$elemento].'\' WHERE ID='.$cache_user->ID);
		}else if($elemento=='ID' or $elemento=='id_cliente'){

		}else{
			mysqli_query($enlace, 'UPDATE sme_usuarios SET '.$elemento.'=\'\' WHERE ID='.$cache_user->ID);
		}
		next($mapa);
	}

	header('Location: '.$retorno);
}
?>