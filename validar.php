<?php
session_start();
include_once('modelos/Tools.php');

$enlace = Tools::conexion();

function validar($usuario, $contrasena){
	$enlace = Tools::conexion();
	$nombre=mysqli_real_escape_string($enlace, filter_var($usuario, FILTER_SANITIZE_STRING));
	$password=mysqli_real_escape_string($enlace, filter_var($contrasena, FILTER_SANITIZE_STRING));

	$consulta=mysqli_query($enlace, 'select * from sme_usuarios where usuario=\''.$nombre.'\' and contrasena=\''.$password.'\'');

	$i=0;
	if($consulta){

		while($fetch=mysqli_fetch_array($consulta)){
			$_SESSION['id_usuario']=$fetch['ID'];
			$i++;
		}
	}

	return $i;
}

if(!isset($_POST['nombre']) or !isset($_POST['contrasena'])){
	header('Location: ./');
}else{
	if(validar($_POST['nombre'], $_POST['contrasena'])!=0){
		$nombre=mysqli_real_escape_string($enlace, filter_var($_POST['nombre'], FILTER_SANITIZE_STRING));
		$contrasena=mysqli_real_escape_string($enlace, filter_var($_POST['contrasena'], FILTER_SANITIZE_STRING));

		$_SESSION['usuario']=$nombre;
		$_SESSION['contrasena']=$contrasena;

		$consulta=mysqli_query($enlace, 'select * from sme_usuarios where usuario="'.$_SESSION['usuario'].'"');
		while($fila=mysqli_fetch_array($consulta)){
			$_SESSION['id']=$fila['ID'];
			$_SESSION['id_cliente']=$fila['id_cliente'];
			if($fila['superadmin']==1){
				$_SESSION['admin']=1;
			}
		}

		if(isset($_SESSION['registro_rapido'])){
			$auxiliar=$_SESSION['registro_rapido'];
			unset($_SESSION['registro_rapido']);

			header('Location: '.$auxiliar);
		}else{
			header('Location: ./');
		}

	}else{
		setcookie('error_log', '1', time()+60*60*24*365, '/');
		header('Location: ./login.php');
	}
}
?>