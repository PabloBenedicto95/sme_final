<?php
session_start();
include('modelos/Tools.php');
if(!isset($_POST['nombre']) || $_POST['nombre']==''){
	setcookie('error_log', '1', time()+60*60*24*365, '/');
	if(isset($_COOKIE['retorno'])){
		$retorno=$_COOKIE['retorno'];
		setcookie('retorno', '', -1, '/');
		header('Location: '.$retorno);
	}else{
		header('Location: ./');
	}
}else{
	$cache_cliente=json_decode(Tools::desencriptar($_COOKIE['cache_cliente']));
	setcookie('cache_cliente', '', -1, '/');

	if($_SESSION['id_cliente']!=$cache_cliente->ID){
		header('Location: ./');
	}

	$retorno=$_COOKIE['retorno'];
	setcookie('retorno', '', -1, '/');

	if(!isset($_POST['nombre']) or  !isset($_POST['email']) or !isset($_POST['telefono'])){
		header("Location: ./".$retorno);
	}

	$array=array('nombre'=>filter_input(INPUT_POST, 'nombre',FILTER_SANITIZE_EMAIL), 'email'=>filter_input(INPUT_POST, 'email',FILTER_SANITIZE_EMAIL), 'telefono'=>filter_input(INPUT_POST, 'telefono',FILTER_SANITIZE_NUMBER_INT));

	$enlace=Tools::conexion();

	$mapa=array_keys(get_object_vars($cache_cliente));
	$llaves=array_keys($array);

	foreach($mapa as $elemento){
		$esta=false;
		for($i=0; $i<count($llaves); $i++){
			if($llaves[$i]==$elemento){
				$esta=true;
				break;
			}
		}

		if($esta){
			mysqli_query($enlace, 'UPDATE sme_clientes SET '.$elemento.'=\''.$array[$elemento].'\' WHERE ID='.$cache_cliente->ID);
		}else if($elemento=='ID'){
		//No hace nada, ya que estas opciones se quedan de forma predeterminada guardadas
		}else{
			mysqli_query($enlace, 'UPDATE sme_clientes SET '.$elemento.'=\'\' WHERE ID='.$cache_cliente->ID);
		}
		next($mapa);
	}

	header('Location: '.$retorno);
}
?>