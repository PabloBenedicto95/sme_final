<?php
session_start();
include 'modelos/Tools.php';

if(!isset($_POST['email'])){
	setcookie("fallo_email","red",time()+60*60*24*365,"/");
	header("Location: recuperacion_contrasena.php");
}

$enlace=Tools::conexion();

$email=mysqli_escape_string($enlace, filter_var($_POST['email'], FILTER_SANITIZE_EMAIL));

if($email!=''){
	$consulta=mysqli_query($enlace, 'SELECT * FROM sme_usuarios WHERE email="'.$email.'"');

	if(mysqli_num_rows($consulta)!=0){
		while($fila=mysqli_fetch_array($consulta)){
			$nueva_contrasena=substr(md5(microtime()), 1, 15);

			$consulta_interna=mysqli_query($enlace, 'UPDATE sme_usuarios SET contrasena=\''.$nueva_contrasena.'\' WHERE ID='.$fila['ID']);

			$para      = $email;
			$titulo    = 'Restauración de contraseña';
			$mensaje   = "Este es un email automático para restablecer la contraseña. Si usted no ha solicitado este email póngase en contacto con CASFID (contacto@casfid.es). Hemos restaurado la contraseña para el usuario ".$fila['usuario'].", a continuación le dejamos la nueva contraseña para que la introduzca al iniciar sesión.\n\n\n\n\n\n".$nueva_contrasena."\n\n\n\nCASFID contacto@casfid.es";
			$cabeceras = 'From: sme@casfid.es' . "\r\n" .
			'Reply-To: sme@casfid.es' . "\r\n" .
			'X-Mailer: PHP/' . phpversion();
			error_reporting(0);
			mail($para, $titulo, $mensaje, $cabeceras);
			error_reporting(-1);
			if($consulta_interna){
				$_SESSION['email_enviado']='Email enviado';
			}
		}
	}else{
		setcookie("fallo_email","red",time()+60*60*24*365,"/");
	}
}else{
	setcookie("fallo_email","red",time()+60*60*24*365,"/");
}
//header('Location: recuperacion_contrasena.php');
?>