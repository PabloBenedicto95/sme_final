<?php
session_start();
if(!isset($_SESSION['usuario'])){
	header('Location: ./');
}

$todoOK=true;

include('modelos/Tools.php');
$enlace = Tools::conexion();

if($_POST['contrasena1']!=$_POST['contrasena2']){
	setcookie("fallo_contrasena","red",time()+60*60*24*365,"/");
	$todoOK=false;
}else{
	$contrasena=mysqli_real_escape_string($enlace, $_POST['contrasena1']);
}

if(isset($_POST['email']) and $_POST['email']!=''){
	if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)!=''){
		$email=mysqli_real_escape_string($enlace, filter_var($_POST['email'], FILTER_SANITIZE_EMAIL));
	}else{
		setcookie("fallo_email","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}else{
	setcookie("fallo_email","red",time()+60*60*24*365,"/");
	$todoOK=false;
}

if(isset($_POST['nombre'])){
	if(filter_var($_POST['nombre'], FILTER_SANITIZE_EMAIL)!=''){
		$nombre=mysqli_real_escape_string($enlace, filter_var($_POST['nombre'], FILTER_SANITIZE_EMAIL));
	}else{
		setcookie("fallo_nombre","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}

if(isset($_POST['usuario'])){
	if(filter_var($_POST['usuario'], FILTER_SANITIZE_EMAIL)!=''){
		$usuario=mysqli_real_escape_string($enlace, filter_var($_POST['usuario'], FILTER_SANITIZE_EMAIL));

		$consulta=mysqli_query($enlace, 'select * from sme_usuarios');
		while($fila=mysqli_fetch_array($consulta)){
			if($fila['usuario']==$usuario){
				setcookie("fallo_usuario","red",time()+60*60*24*365,"/");
				setcookie("usuario_duplicado","Nombre de usuario ya en uso",time()+60*60*24*365,"/");
				$todoOK=false;
				break;
			}
		}
	}else{
		setcookie("fallo_usuario","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}

if(isset($_POST['dni']) and $_POST['dni']!=''){
	if(filter_var($_POST['dni'], FILTER_SANITIZE_EMAIL)!=''){
		$dni=mysqli_real_escape_string($enlace, filter_var($_POST['dni'], FILTER_SANITIZE_EMAIL));
	}else{
		setcookie("fallo_dni","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}

if(isset($_POST['telefono']) and $_POST['telefono']!=''){
	if(filter_var($_POST['telefono'], FILTER_VALIDATE_INT)){
		$telefono=mysqli_real_escape_string($enlace, filter_var($_POST['telefono'], FILTER_SANITIZE_NUMBER_INT));
	}else{
		setcookie("fallo_telefono","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}

if((isset($_POST['cliente']) and $_POST['cliente']!='') and $_SESSION['id_cliente']==$_POST['cliente']){
	if(filter_var($_POST['cliente'], FILTER_SANITIZE_EMAIL)){
		$cliente=mysqli_real_escape_string($enlace, filter_var($_SESSION['id_cliente'], FILTER_SANITIZE_NUMBER_INT));
	}else{
		setcookie("fallo_cliente","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}else if((isset($_SESSION['admin']) and isset($_POST['cliente'])) and $_POST['cliente']){
	if(filter_var($_POST['cliente'], FILTER_SANITIZE_EMAIL)){
		$cliente=mysqli_real_escape_string($enlace, filter_var($_POST['cliente'], FILTER_SANITIZE_NUMBER_INT));
	}else{
		setcookie("fallo_cliente","red",time()+60*60*24*365,"/");
		$todoOK=false;
	}
}

if($todoOK){
	$consulta='insert into sme_usuarios (contrasena';

	if(isset($email)){
		$consulta.=', email';
	}

	if(isset($telefono)){
		$consulta.=', telefono';
	}

	if(isset($usuario)){
		$consulta.=', usuario';
	}

	if(isset($nombre)){
		$consulta.=', nombre';
	}

	if(isset($dni)){
		$consulta.=', dni';
	}

	if(isset($cliente)){
		$consulta.=', id_cliente';
	}

	$consulta.=') values ("'.$contrasena.'"';

	if(isset($email)){
		$consulta.=', "'.$email.'"';
		unset($email);
	}

	if(isset($telefono)){
		$consulta.=', "'.$telefono.'"';
		unset($telefono);
	}

	if(isset($usuario)){
		$consulta.=', "'.$usuario.'"';
		unset($usuario);
	}

	if(isset($nombre)){
		$consulta.=', "'.$nombre.'"';
		unset($nombre);
	}

	if(isset($dni)){
		$consulta.=', "'.$dni.'"';
		unset($dni);
	}

	if(isset($cliente)){
		$consulta.=', "'.$cliente.'"';
		unset($cliente);
	}

	$consulta.=')';

	$accion=mysqli_query($enlace, $consulta);

	if($accion){
		$_SESSION['registro_completo']='El usuario '.$_POST['nombre'].' ha sido creado con éxito';
	}

	header('Location: anadir_usuario.php');
}else{
	header('Location: anadir_usuario.php');
}
?>