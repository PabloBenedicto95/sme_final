<!DOCTYPE html>
<html lang="en">
<head>
	<?php
	session_start();
	include 'modelos/Tools.php';

	$enlace=Tools::crearHeaderYConexion();

	if(!isset($_GET['id'])){
		header('Location: ./');
	}

	$numero=mysqli_real_escape_string($enlace, filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT));
	$consulta=mysqli_query($enlace, 'select * from sme_eventos where ID='.$numero);

	if(mysqli_num_rows($consulta)==0){
		header('Location: ./');
	}else{
		Tools::crearCookieNumero($numero);
	}

	$fila=mysqli_fetch_assoc($consulta);

	if(!isset($_SESSION['admin'])){
		if($_SESSION['id_cliente']!=$fila['id_cliente']){
			header('Location: ./');
		}
	}

	$_SESSION['evento']=$fila;

	if(!isset($_SESSION['usuario'])){
		$_SESSION['registro_rapido']=(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	}else{
		if(isset($_SESSION['registro_rapido'])){
			unset($_SESSION['registro_rapido']);
		}
	}

	?>
	<title><?php
	echo $_SESSION['evento']['nombre'];
	?> I Eventos</title>

	<style type="text/css">
	
	.user-online-status{
		<?php
		if(isset($_SESSION['usuario'])){
			echo 'background: green !important;';
		}else{
			echo 'display: none;';
		}
		?>
	}

</style>

<!--alerts CSS -->
<link href="vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">
</head>

<body>
	<!-- Preloader -->
	<?php
	Tools::preloader();
	?>
	<!-- /Preloader -->
	<div class="wrapper theme-1-active pimary-color-blue">
		<!-- Top Menu Items -->
		<?php
		Tools::crearMenuTop();
		?>
		<!-- /Top Menu Items -->
		
		<!-- Left Sidebar Menu -->
		<?php
		Tools::crearMenuLeft();
		?>
		<!-- /Left Sidebar Menu -->


		<!-- Main Content -->
		<div class="page-wrapper">
			<div class="container-fluid">
				<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
						<ol class="breadcrumb">
							<li title="Volver a la página principal"><a href="index.html">Inicio</a></li>
							<li title="Cliente"><a <?php
							$consulta=mysqli_query($enlace, 'select * from sme_eventos where ID='.$numero);
							while($fila=mysqli_fetch_assoc($consulta)){
								$cliente_id=$fila['id_cliente'];
							}
							if($cliente_id){
								$consulta_clientes=mysqli_query($enlace, 'select * from sme_clientes where ID='.$cliente_id);
								while($fila=mysqli_fetch_assoc($consulta_clientes)){
									$nombre_cliente=$fila['nombre'];
								}

								echo 'href="cliente.php?id='.$cliente_id.'">'.$nombre_cliente;
							}else{
								echo '>*Sin cliente asignado*';
							}
							?></a></li>
							<li class="active"><span><?php
							$consulta=mysqli_query($enlace, 'select * from sme_eventos where ID='.$numero);
							$resultado=mysqli_fetch_assoc($consulta);

							echo $resultado['nombre'];
							?></span></li>
						</ol>
					</div>
					<!-- /Breadcrumb -->
				</div>
				<!-- /Title -->

				<!--Botón y formulario modificar evento-->
				<?php
				Tools::formulario_modificar_evento($numero, $enlace);
				?>

				<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="row">
										<?php
										$consulta=mysqli_query($enlace, 'select * from sme_eventos where ID='.$numero);
										$resultado=mysqli_fetch_assoc($consulta);
										error_reporting(1);
										if(file_get_contents($resultado['imagen'])){
											$imagen_carga=true;
										}else{
											$imagen_carga=false;
										}
										error_reporting(-1);

										if($imagen_carga){
											echo '<div class="col-md-3">
											<div class="item-big">
											<!-- START carousel-->
											<div id="carousel-example-captions-1" data-ride="carousel" class="carousel slide">
											<ol class="carousel-indicators">
											<li data-target="#carousel-example-captions-1" data-slide-to="0" class="active"></li>
											</ol><div role="listbox" class="carousel-inner">
											<div class="item active"> <img src="'.$resultado['imagen'].'"alt="First slide image"></div></div></div>

											<!-- END carousel-->
											</div>
											</div>';
										}
										?>
										
										<div class="col-md-9">
											<div class="product-detail-wrap">
												<h3 class="mb-20 weight-500"><?php
												$consulta=mysqli_query($enlace, 'select * from sme_eventos where ID='.$numero);
												$resultado=mysqli_fetch_assoc($consulta);

												echo $resultado['nombre'];
												?></h3>
												<div class="product-price head-font mb-30"><?php

												$consulta=mysqli_query($enlace, 'select * from sme_eventos where ID='.$numero);
												$meses = array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
												$fila=mysqli_fetch_assoc($consulta);

												$array=str_split($fila['fecha']);

												$fecha=$array[8].$array[9].' de ';

												if($array[5]!='0'){
													$fecha.=$meses[(intval($array[6])+10)-1].' del ';
												}else if((intval($array[6]))-1==(-1)){

												}else{
													$fecha.=$meses[(intval($array[6]))-1].' del ';
												}

												$fecha.=$array[0].$array[1].$array[2].$array[3];

												echo $fecha;

												?></div>
												<p class="mb-50">Lorem ipsum dolor sit amet, facer velit at per, possit accusamus vim an. Cu vel possit dolorum. Elit placerat molestiae mea ne.Ex has movet ornatus, ei usu latine scripta molestiae. Sea ex aeterno adversarium, te ferri errem noluisse quo, meis civibus ea est. Sit in quas nostrud.</p>
												
												<?php
												if(isset($_SESSION['admin'])){
													echo '<div class="btn-group mr-10">
													<button class="btn btn-danger btn-lable-wrap left-label" id="r_eventos">
													<span class="btn-label">
													<i class="fa fa-exclamation-triangle"></i>
													</span>
													<span class="btn-text">Eliminar</span>
													</button>

													<script type="text/javascript">
													document.getElementById(\'r_eventos\').onclick=redireccionar;

													function redireccionar(){
														if(confirm(\'¿Seguro que quiere eliminar este evento?\n\n\nEsta opción no se puede deshacer.\')){
															if(confirm(\'¿Estás seguro? Vas a eliminar de forma permanente *'.$_SESSION['evento']['nombre'].'*\')){
																location.href = "eliminar_evento.php?id=';
																echo $numero;
																echo '";
															}
														}
													}
													</script>
													</div>';
												}
												?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Row -->
				
				<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div  class="tab-struct custom-tab-1 product-desc-tab">
										<ul role="tablist" class="nav nav-tabs nav-tabs-responsive" id="myTabs_7">
											<li class="active" role="presentation"><a aria-expanded="true"  data-toggle="tab" role="tab" id="descri_tab" href="#descri_tab_detail"><span>Características</span></a></li>
											<li role="presentation" class="next"><a  data-toggle="tab" id="adi_info_tab" role="tab" href="#adi_info_tab_detail" aria-expanded="false"><span>Información adicional</span></a></li>
											<li role="presentation" class="last"><a  data-toggle="tab" id="adi_info_tab" role="tab" href="#estadisticas" aria-expanded="false"><span>Estadísticas</span></a></li>
										</ul>
										<div class="tab-content" id="myTabContent_7">
											<div  id="descri_tab_detail" class="tab-pane fade active in pt-0" role="tabpanel">
												<div class="table-wrap">
													<div class="table-responsive">
														<table class="table  mb-0">
															<tbody>
																<tr>
																	<td class="border-none">Mostrar comprobación de la pulsera</td>
																	<td class="border-none"><?php
																	if($_SESSION['evento']['mostrar_comprobar_pulsera']){
																		echo '<p style="color: green;"><i class="fa fa-check"></i> Activado</p>';
																	}else{
																		echo '<p style="color: orange;"><i class="fa fa-warning"></i> Desactivado</p>';
																	}
																	?></td>
																</tr>
																<tr>
																	<td class="border-none">Registro previo</td>
																	<td class="border-none"><?php
																	if($_SESSION['evento']['registro_previo']){
																		echo '<p style="color: green;"><i class="fa fa-check"></i> Activado</p>';
																	}else{
																		echo '<p style="color: orange;"><i class="fa fa-warning"></i> Desactivado</p>';
																	}
																	?></td>
																</tr>
																<tr>
																	<td class="border-none">Registro por email</td>
																	<td class="border-none"><?php
																	if($_SESSION['evento']['registro_email']){
																		echo '<p style="color: green;"><i class="fa fa-check"></i> Activado</p>';
																	}else{
																		echo '<p style="color: orange;"><i class="fa fa-warning"></i> Desactivado</p>';
																	}
																	?></td>
																</tr>
																<tr>
																	<td class="border-none">Registro por número de teléfono</td>
																	<td class="border-none"><?php
																	if($_SESSION['evento']['registro_telefono']){
																		echo '<p style="color: green;"><i class="fa fa-check"></i> Activado</p>';
																	}else{
																		echo '<p style="color: orange;"><i class="fa fa-warning"></i> Desactivado</p>';
																	}
																	?></td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div  id="adi_info_tab_detail" class="tab-pane pt-0 fade" role="tabpanel">
												<div class="table-wrap">
													<div class="table-responsive">
														<table class="table  mb-0">
															<tbody>
																<tr>
																	<td class="border-none">Lugar</td>
																	<td class="border-none"><?php
																	$consulta=mysqli_query($enlace, 'select * from sme_eventos where ID='.$numero);
																	$resultado=mysqli_fetch_assoc($consulta);

																	if($resultado['lugar']){
																		echo '<a title="Lugar del evento">'.$resultado['lugar'].'</a>';
																	}else{
																		echo 'Sin lugar establecido';
																	}
																	?></td>
																</tr>
																<tr>
																	<td>Web Oficial</td>
																	<td><?php
																	$consulta=mysqli_query($enlace, 'select * from sme_eventos where ID='.$numero);
																	$resultado=mysqli_fetch_assoc($consulta);

																	if($resultado['url']){
																		echo '<a href="'.$resultado['url'].'" title="Web oficial" target="_blank">'.$resultado['url'].'</a>';
																	}else{
																		echo 'Sin web oficial';
																	}
																	?></td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div  id="estadisticas" class="tab-pane pt-0 fade" role="tabpanel">
												<div class="table-wrap">
													<div class="div_estadisticas" id="div_1">
														<h3 style="text-align: center;">Publicaciones de acciones</h3>
														<div id="container"></div>
													</div>

													<div class="div_estadisticas" id="div_2">
														<h3 style="text-align: center;">Participantes: <?php
														$consulta=mysqli_query($enlace, 'SELECT COUNT(*) FROM sme_participantes WHERE id_evento='.$numero);
														echo mysqli_fetch_assoc($consulta)['COUNT(*)'];
														?></h3>
														<canvas id="Distribucion"></canvas>
													</div>

													<div class="div_estadisticas_n" id="div_4">
														<div id="test-char"></div>
													</div>

													<script src="dist/js/highcharts/code/highcharts.js"></script>
													<script src="dist/js/highcharts/code/modules/data.js"></script>
													<script src="dist/js/highcharts/code/modules/drilldown.js"></script>
													<script type="text/javascript" src="./vendors/chart.js/Chart.bundle.min.js"></script>
													<script type="text/javascript">
														<?php
														//Gráfico 'Publicaciones'
														$nombre=array('video_boomerang'=>'Vídeo en boomerang', 'video'=>'Vídeo', 'registro_en_accion'=>'Registro en acción', 'texto_personalizado'=>'Texto personalizado', 'sin_foto'=>'Sin foto', 'etiquetado_multiple'=>'Etiquetado múltiple');
														$datos=array('video_boomerang'=>0, 'video'=>0, 'registro_en_accion'=>0, 'texto_personalizado'=>0, 'sin_foto'=>0, 'etiquetado_multiple'=>0);

														$consulta=mysqli_query($enlace, 'select * from sme_acciones where id_evento='.$numero);

														$acciones_id=array();

														while($fila=mysqli_fetch_array($consulta)){
															array_push($acciones_id, $fila['ID']);
														}

														$publicaciones_de_acciones=array();
														foreach ($acciones_id as $key => $value) {
															$publicaciones_de_acciones[$value]=array();
															$consulta=mysqli_query($enlace, 'SELECT * FROM sme_publicaciones WHERE id_accion='.$value);
															while($fila=mysqli_fetch_array($consulta)){
																array_push($publicaciones_de_acciones[$value], $fila);
															}
														}

														foreach ($publicaciones_de_acciones as $id_accion => $array_publicacion) {
															foreach ($array_publicacion as $key => $value) {
																$accion=mysqli_query($enlace, 'SELECT * FROM sme_acciones WHERE ID='.$id_accion);
																while($fila=mysqli_fetch_array($accion)){
																	foreach($datos as $atributo => $valor){
																		if($fila[$atributo]){
																			$datos[$atributo]++;
																		}
																	}
																}
															}
														}
														?>
														Highcharts.chart('container', {
															chart: {
																type: 'column'
															},
															title: {
																text: 'Acciones del evento'
															},
															subtitle: {
																text: 'Selecciona una columna para ver las acciones de ese tipo'
															},
															xAxis: {
																type: 'category'
															},
															yAxis: {
																title: {
																	text: 'Publicaciones'
																}

															},
															legend: {
																enabled: false
															},
															plotOptions: {
																series: {
																	borderWidth: 0,
																	dataLabels: {
																		enabled: false,
																		format: '{point.y:.1f} - Publicaciones'
																	}
																}
															},

															tooltip: {
																headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
																pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f} - Publicaciones</b> del global<br/>'
															},

															"series": [
															{
																"name": "Tipo de acción",
																"colorByPoint": true,
																"data": [<?php
																	$primera=true;

																	foreach ($datos as $key => $value) {
																		if($primera){
																			$primera=false;
																		}else{
																			echo ', ';
																		}

																		echo '{"name": "'.$nombre[$key].'",
																		"y": '.$value.',
																		"drilldown": "'.$key.'"}';
																	}
																	?>
																	]
																}
																],
																"drilldown": {
																	"series": [<?php
																		$primera=true;

																		foreach ($datos as $key => $value) {
																			if($primera){
																				$primera=false;
																			}else{
																				echo ', ';
																			}

																			echo '{
																				"name": "'.$nombre[$key].'",
																				"id": "'.$key.'",
																				"data": [';

																				$consulta_cache=mysqli_query($enlace, 'SELECT * FROM sme_acciones WHERE id_evento='.$numero.' and '.$key.'!=0');

																				$primera_interna=true;
																				while($fila=mysqli_fetch_array($consulta_cache)){
																					if($primera_interna){
																						$primera_interna=false;
																					}else{
																						echo ', ';
																					}

																					$consulta_interna=mysqli_fetch_assoc(mysqli_query($enlace, 'SELECT COUNT(*) FROM sme_publicaciones WHERE id_accion='.$fila['ID']));

																					echo '[
																					"'.$fila['nombre'].'",
																					'.$consulta_interna['COUNT(*)'].'
																				]';

																			}

																			echo ']}';
																		}
																		?>
																		]
																	}
																});
															</script>

															<script>
																<?php
																//Gráfico 'Distribucion'
																$datos=array('facebook_asociado'=>0, 'media_amigos'=>array(), 'estimacion'=>0);



																$participantes_evento=mysqli_query($enlace, 'SELECT * FROM sme_participantes WHERE id_evento='.$numero);

																while($fila=mysqli_fetch_array($participantes_evento)){

																	array_push($datos['media_amigos'], $fila['numero_amigos']);

																	if($fila['enlace']!=''){
																		$datos['facebook_asociado']++;
																	}
																}

																$media=0;
																for($i=0; $i<count($datos['media_amigos']); $i++){
																	$media+=$datos['media_amigos'][$i];
																}

																$datos['media_amigos']=intval($media/$i);

																$datos['estimacion']=intval(sqrt(intval((($datos['media_amigos']-((10*$datos['media_amigos'])/100))*$datos['facebook_asociado'])/95))*$datos['facebook_asociado']);

																?>
																var char2 = document.getElementById("Distribucion").getContext('2d');
																new Chart(char2, {
																	type: 'horizontalBar',
																	data: {
																		labels: ["Facebook asociado", "Media de amigos en Facebook",  "Estimación de expansión de publicaciones"],
																		datasets: [{
																			label: 'Personas',
																			data: [<?php
																				$primera=true;
																				foreach ($datos as $key => $valor) {
																					if($primera){
																						$primera=false;
																					}else{
																						echo ' ,';
																					}
																					echo $valor;
																				}
																				?>],
																				backgroundColor: [
																				'rgba(25, 99, 82, 1)',
																				'rgba(255, 99, 132, 1)',
																				'rgba(54, 162, 235, 1)'
																				],
																				borderColor: [
																				'rgba(25, 99, 82, 0.1)',
																				'rgba(255, 99, 132, 0.1)',
																				'rgba(54, 162, 235, 0.1)'
																				],
																				borderWidth: 1
																			}]
																		},
																		options: {
																			scales: {
																				yAxes: [{
																					ticks: {
																						beginAtZero:true
																					}
																				}]
																			}
																		}
																	});
																</script>
																<script>
																	<?php
																	//Gráfico 'Número de publicaciones'

																	$array_publicaciones=array();

																	$tabla_resultado=mysqli_query($enlace, 'SELECT * FROM sme_acciones WHERE id_evento='.$numero); 

																	$intervalo=60;

																	$ides=array();
																	while($fila=mysqli_fetch_array($tabla_resultado)){
																		$consulta='SELECT COUNT(*) as cantidad, UNIX_TIMESTAMP(sme_publicaciones.hora) DIV ('.$intervalo.' * 60) AS timekey FROM sme_publicaciones WHERE id_accion='.$fila['ID'].' GROUP BY timekey ORDER BY sme_publicaciones.hora ASC;';
																		$cache=mysqli_query($enlace, $consulta);

																		array_push($array_publicaciones, $cache);
																		array_push($ides, $fila['ID']);
																	}

																	//Consulta para recoger la hora de incio
																	$la_consulta='SELECT p.* FROM sme_publicaciones p WHERE p.id_accion IN(';
																	$primera=true;
																	foreach ($ides as $key => $value) {
																		if($primera){
																			$primera=false;
																		}else{
																			$la_consulta.=', ';
																		}
																		$la_consulta.=$value;
																	}

																	$la_consulta.=') ORDER BY p.hora ASC LIMIT 1;';

																	$result = mysqli_query($enlace, $la_consulta);
																	if(mysqli_num_rows($result) == 1){

																		$row = mysqli_fetch_array($result);
																		$hora_primera_operacion = $row["hora"];

																		//Hora de inicio de la gráfica
																		$inicio = strtotime($hora_primera_operacion);
																		$inicio = $inicio - $inicio % ($intervalo * 60);

																		//Fecha de inicio de la gráfica
																		$fecha_inicio_grafica = $inicio * 1000;
																	}

																	$mostrar=array();
																	foreach ($array_publicaciones as $key => $value) {
																		while($fila=mysqli_fetch_array($value)){
																			array_push($mostrar, array('cantidad'=>$fila['cantidad'], 'timekey'=>$fila['timekey']));
																		}
																	}

																	$cache=array();
																	$primera=true;
																	foreach ($mostrar as $key => $value) {
																		/*if($primera){
																			$primera=false;
																			$d=new DateTime();
																			$n=$d->getOffset();
																			$actual=($fecha_inicio_grafica - $n * 60 * 1000)+intval($value['timekey']);
																		}else{
																			$actual+=intval($value['timekey'])*$intervalo;
																		}
																		array_push($cache, array($actual, intval($value['cantidad'])));*/
																		array_push($cache, array(intval($value['timekey']), intval($value['cantidad'])));
																	}
																	$json_datos=json_encode($cache);

																	?>
																	var d = new Date();
																	var n = d.getTimezoneOffset();

																	Highcharts.chart('test-char', {
																		chart: {
																			zoomType: 'x'
																		},
																		title: {
																			text: 'Cantidad de publicaciones por tiempo'
																		},
																		subtitle: {
																			text: document.ontouchstart === undefined ?
																			'Haga clic y arrastre en el área del gráfico para ampliar' : 'Pase por encima el ratón para ver un dato'
																		},
																		xAxis: {
																			type: 'datetime'
																		},
																		yAxis: {
																			title: {
																				text: 'Núm. de publicaciones'
																			}
																		},
																		legend: {
																			enabled: false
																		},
																		plotOptions: {
																			area: {
																				fillColor: {
																					linearGradient: {
																						x1: 0,
																						y1: 0,
																						x2: 0,
																						y2: 1
																					},
																					stops: [
																					[0, Highcharts.getOptions().colors[0]],
																					[1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
																					]
																				},
																				marker: {
																					radius: 2
																				},
																				lineWidth: 1,
																				states: {
																					hover: {
																						lineWidth: 1
																					}
																				},
																				threshold: null
																			}
																		},

																		series: [{
																			type: 'column',
																			pointInterval: <?php echo $intervalo * 60 * 1000; ?>,
																			pointStart: <?php echo $fecha_inicio_grafica; ?> - n * 60 * 1000,
																			name: 'Publicaciones',
																			data: <?php
																			echo $json_datos;
																			?>
																		}]
																	});
																	/*var date = new Date((<?php echo $fecha_inicio_grafica; ?> - n * 60 * 1000));

																	var dia=date.getDay();
																	var mes=date.getMonth();
																	var ano=date.getYear();
																	// Hours part from the timestamp
																	var hours = date.getHours();
																	// Minutes part from the timestamp
																	var minutes = "0" + date.getMinutes();
																	// Seconds part from the timestamp
																	var seconds = "0" + date.getSeconds();

																	alert(dia+'/'+mes+'/'+ano+' a las '+hours+':'+minutes+':'+seconds);*/
																</script>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /Row -->
						</div>

						<!-- Footer -->
						<?php
						Tools::crearFooter();
						?>
						<!-- /Footer -->

					</div>
					<!-- /Main Content -->

				</div>
				<!-- /#wrapper -->

				<!-- JavaScript -->

				<!-- jQuery -->
				<script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>

				<!-- Bootstrap Core JavaScript -->
				<script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

				<!-- Data table JavaScript -->
				<script src="vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>

				<!-- Slimscroll JavaScript -->
				<script src="dist/js/jquery.slimscroll.js"></script>

				<!-- Progressbar Animation JavaScript -->
				<script src="vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
				<script src="vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>

				<!-- Fancy Dropdown JS -->
				<script src="dist/js/dropdown-bootstrap-extended.js"></script>

				<!-- Sparkline JavaScript -->
				<script src="vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>

				<!-- Owl JavaScript -->
				<script src="vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>

				<!-- Switchery JavaScript -->
				<script src="vendors/bower_components/switchery/dist/switchery.min.js"></script>

				<!-- EChartJS JavaScript -->
				<script src="vendors/bower_components/echarts/dist/echarts-en.min.js"></script>
				<script src="vendors/echarts-liquidfill.min.js"></script>

				<!-- Scripts para los datapickers y los modales-->
				<!-- Moment JavaScript -->
				<script type="text/javascript" src="vendors/bower_components/moment/min/moment-with-locales.min.js"></script>

				<!-- Bootstrap Colorpicker JavaScript -->
				<script src="vendors/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>

				<!-- Switchery JavaScript -->
				<script src="vendors/bower_components/switchery/dist/switchery.min.js"></script>

				<!-- Select2 JavaScript -->
				<script src="vendors/bower_components/select2/dist/js/select2.full.min.js"></script>

				<!-- Bootstrap Select JavaScript -->
				<script src="vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

				<!-- Bootstrap Tagsinput JavaScript -->
				<script src="vendors/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

				<!-- Bootstrap Touchspin JavaScript -->
				<script src="vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>

				<!-- Multiselect JavaScript -->
				<script src="vendors/bower_components/multiselect/js/jquery.multi-select.js"></script>

				<!-- Bootstrap Switch JavaScript -->
				<script src="vendors/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>

				<!-- Bootstrap Datetimepicker JavaScript -->
				<script type="text/javascript" src="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

				<!-- Form Advance Init JavaScript -->
				<script src="dist/js/form-advance-data.js"></script>

				<!-- Slimscroll JavaScript -->
				<script src="dist/js/jquery.slimscroll.js"></script>

				<!-- Fancy Dropdown JS -->
				<script src="dist/js/dropdown-bootstrap-extended.js"></script>

				<!-- Owl JavaScript -->
				<script src="vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>

				<!-- Toast JavaScript -->
				<script src="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>

				<!--Moment JavaScript-->
				<script type="vendors/moment.js/moment.min.js"></script>

				<!-- Sweet-Alert  -->
				<script src="vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>

				<script src="dist/js/sweetalert-data.js"></script>

				<!-- Init JavaScript -->
				<script src="dist/js/init.js"></script>
				<?php

				if(isset($_SESSION['registro_completo'])){
						echo '<script src="dist/js/dashboard-data.js"></script>';

					unset($_SESSION['registro_completo']);
				}

				?>
				<?php
				unset($_SESSION['evento']);
				?>
			</body>

			</html>