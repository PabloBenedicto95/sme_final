<!DOCTYPE html>
<html lang="en">
<head>
	<?php
	session_start();

	if(!isset($_SESSION['usuario'])){
		header('Location: ./');
	}

	include 'modelos/Tools.php';

	$enlace=Tools::crearHeaderYConexion();

	if(isset($_COOKIE['fallo_nombre'])){
		$fallo_nombre=$_COOKIE['fallo_nombre'];
		setcookie("fallo_nombre","",(-1),"/");
	}
	if(isset($_COOKIE['fallo_usuario'])){
		$fallo_usuario=$_COOKIE['fallo_usuario'];
		setcookie("fallo_usuario","",(-1),"/");
	}
	if(isset($_COOKIE['fallo_telefono'])){
		$fallo_telefono=$_COOKIE['fallo_telefono'];
		setcookie("fallo_telefono","",(-1),"/");
	}
	if(isset($_COOKIE['fallo_email'])){
		$fallo_email=$_COOKIE['fallo_email'];
		setcookie("fallo_email","",(-1),"/");
	}
	if(isset($_COOKIE['fallo_dni'])){
		$fallo_dni=$_COOKIE['fallo_dni'];
		setcookie("fallo_dni","",(-1),"/");
	}
	if(isset($_COOKIE['fallo_contrasena'])){
		$fallo_contrasena=$_COOKIE['fallo_contrasena'];
		setcookie("fallo_contrasena","",(-1),"/");
	}
	if(isset($_COOKIE['usuario_duplicado'])){
		$usuario_duplicado='<script type="text/javascript">document.getElementById(\'input_usuario\').placeholder="'.$_COOKIE['usuario_duplicado'].'";</script>';
		setcookie("usuario_duplicado","",(-1),"/");
	}
	if(isset($_COOKIE['cookie_id_cliente'])){
		$cookie_id_cliente=$_COOKIE['cookie_id_cliente'];
		setcookie("cookie_id_cliente","",(-1),"/");
	}
	?>

	<title>Crear un nuevo usuario</title>

</head>

<body>
	<!--Preloader-->
	<?php
	Tools::preloader();
	?>
	<!--/Preloader-->
	<div class="wrapper theme-1-active pimary-color-blue">
		
		<!-- Top Menu Items -->
		<?php
		Tools::crearMenuTop();
		?>
		<!-- /Top Menu Items -->
		
		<!-- Left Sidebar Menu -->
		<?php
		Tools::crearMenuLeft();
		?>
		<!-- /Left Sidebar Menu -->

		<!-- Main Content -->
		<div class="page-wrapper">
			<div class="container-fluid pt-25">
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default card-view">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark">Nuevo usuario</h6>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">
									<div class="row">
										<div class="col-md-12">
											<div class="form-wrap">
												<form data-toggle="validator" role="form" method="post" action="introducir_usuario.php">
													<?php
													echo '<script type="text/javascript">
													function comprobar(evento){
														var input=evento.target;

														if(input.value.split("").length==0){
															input.style.borderColor="red";
															}else{
																input.style.borderColor="";
															}
														}
														</script>';
														?>
														<div class="form-group">
															<label for="inputName" class="control-label mb-10">Nombre(*)</label>
															<input type="text" class="form-control" id="input_nombre" placeholder="Nombre" name="nombre" required>
														</div>
														<?php
														if(isset($fallo_nombre)){
															echo '<script type="text/javascript">
															document.getElementById("input_nombre").oninput=comprobar;
															document.getElementById("input_nombre").style.borderColor="'.$fallo_nombre.'";
															</script>';
														}
														?>
														<div class="form-group">
															<label for="inputName" class="control-label mb-10">Usuario(*)</label>
															<input type="text" class="form-control" id="input_usuario" placeholder="Usuario" name="usuario" required>
														</div>
														<?php
														if(isset($fallo_usuario)){
															echo '<script type="text/javascript">
															document.getElementById("input_usuario").oninput=comprobar;
															document.getElementById("input_usuario").style.borderColor="'.$fallo_usuario.'";
															</script>';
														}
														?>
														<div class="form-group">
															<label for="inputName" class="control-label mb-10">Teléfono</label>
															<input type="text" class="form-control" id="telefono" name="telefono" placeholder="Teléfono">
														</div>
														<script type="text/javascript">
															document.getElementById('telefono').oninput=cambioNumero;

															function cambioNumero(evento){
																var input=evento.target;

																input.value=parseInt(input.value);
																if(input.value=='NaN'){
																	input.value='';
																}

																var array=input.value.split('');

																if(array.length==9){
																	var mostrar='';
																	for(var i=0; i<3; i++){
																		mostrar+=array[i];
																	}
																	mostrar+='';

																	for(var i=3; i<9; i++){
																		mostrar+=array[i];
																		if(i==5){
																			mostrar+='';
																		}
																	}
																	input.value=mostrar;
																}else if(array.length>9){
																	var mostrar='';
																	for(var i=0; i<9; i++){
																		mostrar+=array[i];
																	}
																	input.value=mostrar;
																	cambioNumero(evento);
																}
															}
														</script>
														<?php
														if(isset($fallo_telefono)){
															echo '<script type="text/javascript">
															document.getElementById("telefono").oninput=comprobar;
															document.getElementById("telefono").style.borderColor="'.$fallo_telefono.'";
															</script>';
														}
														?>
														<div class="form-group">
															<label for="inputName" class="control-label mb-10">E-mail</label>
															<input type="email" class="form-control" id="input_email"  name="email" placeholder="E-mail">
														</div>
														<?php
														if(isset($fallo_email)){
															echo '<script type="text/javascript">
															document.getElementById("input_email").oninput=comprobar;
															document.getElementById("input_email").style.borderColor="'.$fallo_email.'";
															</script>';
														}
														?>
														<div class="form-group" <?php
														if(!isset($_SESSION['admin'])){
															echo ' style="display: none;"';
														}
														?>>
															<label for="inputName" class="control-label mb-10">Cliente</label>
															<select class="form-control" id="exampleInputuname_1" name="cliente">
																<option value="">--Ninguno--</option>
																<?php
																$consulta=mysqli_query($enlace, 'select * from sme_clientes');

																while($fila=mysqli_fetch_array($consulta)){
																	echo '<option value="'.$fila['ID'].'"';
																	if(($fila['ID']==$_SESSION['id_cliente']) or (isset($_SESSION['admin']) and $cookie_id_cliente==$fila['ID'])){
																		echo ' selected';
																	}
																	echo'>'.$fila['nombre'].'</option>';
																}
																?>
															</select>
														</div>
														<div class="form-group">
															<label for="inputName" class="control-label mb-10">DNI</label>
															<input type="text" class="form-control" id="input_dni" name="dni" placeholder="DNI">
														</div>
														<?php
														if(isset($fallo_dni)){
															echo '<script type="text/javascript">
															document.getElementById("input_dni").oninput=comprobar;
															document.getElementById("input_dni").style.borderColor="'.$fallo_dni.'";
															</script>';
														}
														?>
														<div class="form-group">
															<label for="inputPassword" class="control-label mb-10">Contraseña(*)</label>
															<div class="row">
																<div class="form-group col-sm-12">
																	<input type="password" data-minlength="6" class="form-control contrasenas" id="inputPassword" placeholder="Contraseña" name="contrasena1" required>
																	<div class="help-block">Mínimo 6 carácteres</div>
																</div>
																<div class="form-group col-sm-12">
																	<input type="password" class="form-control contrasenas" id="inputPasswordConfirm" data-match="#inputPassword" data-match-error="Whoops, these don't match" placeholder="Confirma" name="contrasena2" required>
																	<div class="help-block with-errors"></div>
																</div>
															</div>
														</div>
														<?php
														if(isset($fallo_contrasena)){
															echo '<script type="text/javascript">
															document.getElementById("inputPassword").oninput=comprobar;
															document.getElementById("inputPassword").style.borderColor="'.$fallo_contrasena.'";

															document.getElementById("inputPasswordConfirm").oninput=comprobar;
															document.getElementById("inputPasswordConfirm").style.borderColor="'.$fallo_contrasena.'";
															</script>';
														}
														?>
														<div class="form-group mb-0">
															<button type="submit" class="btn btn-success btn-anim"><i class="icon-rocket"></i><span class="btn-text">Crear usuario</span></button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Footer -->
				<?php
				Tools::crearFooter();
				?>
				<!-- /Footer -->

				<!-- /Main Content -->

			</div>
			<!-- /#wrapper -->

			<!-- JavaScript -->

			<!-- jQuery -->
			<script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>

			<!-- Bootstrap Core JavaScript -->
			<script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

			<!-- Data table JavaScript -->
			<script src="vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>

			<!-- Slimscroll JavaScript -->
			<script src="dist/js/jquery.slimscroll.js"></script>

			<!-- Progressbar Animation JavaScript -->
			<script src="vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
			<script src="vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>

			<!-- Fancy Dropdown JS -->
			<script src="dist/js/dropdown-bootstrap-extended.js"></script>

			<!-- Sparkline JavaScript -->
			<script src="vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>

			<!-- Owl JavaScript -->
			<script src="vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>

			<!-- Switchery JavaScript -->
			<script src="vendors/bower_components/switchery/dist/switchery.min.js"></script>

			<!-- EChartJS JavaScript -->
			<script src="vendors/bower_components/echarts/dist/echarts-en.min.js"></script>
			<script src="vendors/echarts-liquidfill.min.js"></script>

			<!-- Toast JavaScript -->
			<script src="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>

			<!-- Init JavaScript -->
			<script src="dist/js/init.js"></script>
			<?php
				if(isset($_SESSION['registro_completo'])){
					echo '<script src="dist/js/dashboard-data.js"></script>';

					unset($_SESSION['registro_completo']);
				}
			?>

			<?php
			if(isset($usuario_duplicado)){
				echo $usuario_duplicado;
				unset($usuario_duplicado);
			}
			?>

		</body>
		<?php
		unset($_SESSION['cache_user']);
		?>
		</html>
