<?php
session_start();

if(!isset($_SESSION['usuario']) || !isset($_SESSION['evento'])){
	if(!isset($_SESSION['evento'])){
		header('Location: ./');
	}

	if(!isset($_SESSION['usuario'])){
		$_SESSION['registro_rapido']=(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		header('Location: ./login.php');
	}
}else{
	include_once('modelos/Tools.php');
	$enlace = Tools::conexion();

	$consulta_asistencia=mysqli_query($enlace, 'select * from usuarios_en_eventos where id_usuario='.$_SESSION['id_usuario'].' and id_evento='.$_SESSION['evento']['ID']);
	$array_asistencia=mysqli_fetch_array($consulta_asistencia);

	if(count($array_asistencia)!=0){
		echo '<script type="text/javascript">alert(\'No puedes registrarte en el evento '.$_SESSION['evento']['nombre'].' porque ya estás registrado. Te llevamos a la página principal\'); location.href=\'./\';</script>';
	}else{
		if($_SESSION['evento']['registro_previo']==1){
			header('Location: ./registrarse.php');
		}

		$id_evento=$_SESSION['evento']['ID'];

		$accion=mysqli_query($enlace, 'insert into usuarios_en_eventos (id_evento, id_usuario) values ("'.$id_evento.'", "'.$_SESSION['id_usuario'].'")');

		$_SESSION['registro_completo']='Te has registrado en el evento \''.$_SESSION['evento']['nombre'].'\'';

		unset($_SESSION['evento']);

		header('Location: ./');
	}
}

?>