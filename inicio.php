<!DOCTYPE html>
<html lang="en">
<head>
	<?php
	session_start();
	include 'modelos/Tools.php';

	$enlace=Tools::crearHeaderYConexion();

	if(isset($_SESSION['evento'])){
		unset($_SESSION['evento']);
	}

	if(isset($_SESSION['registro_rapido'])){
		unset($_SESSION['registro_rapido']);
	}
	?>

	<title>Briefing APP I Android SME</title>
</head>

<body>
	<!-- Preloader -->
	<?php
	Tools::preloader();
	?>
	<!-- /Preloader -->
	<div class="wrapper theme-1-active pimary-color-blue">
		<!-- Top Menu Items -->
		<?php
		Tools::crearMenuTop();
		?>
		<!-- /Top Menu Items -->
		
		<!-- Left Sidebar Menu -->
		<?php
		Tools::crearMenuLeft();
		?>
		<!-- /Left Sidebar Menu -->
		

		<!-- Main Content -->
		<div class="page-wrapper">
			<div class="container-fluid pt-25">

				<?php
				if(isset($_SESSION['admin'])){
					$consulta=mysqli_query($enlace, 'select * from sme_eventos');
				}else{
					$consulta=mysqli_query($enlace, 'select * from sme_eventos where id_cliente='.$_SESSION['id_cliente']);
				}

				Tools::cartel_eventos($consulta);
				?>

				<!--Botón añadir evento-->
				<a href="nuevo_evento.php" style="position: fixed; right: 2%; bottom: 5%; z-index: 999;">
					<button class="btn btn-warning btn-icon-anim btn-square" title="Crear un evento">
						<div data-icon="H" class="linea-icon linea-elaborate"></div>
					</button>
				</a>

				
				<!-- Footer -->
				<?php
				Tools::crearFooter();
				?>
				<!-- /Footer -->
			</div>
		</div>
		<!-- /Main Content -->

	</div>
	<!-- /#wrapper -->
	
	<!-- JavaScript -->
	
	<?php
	Tools::crearJavascript();
	?>
	<?php
		if(isset($_SESSION['registro_completo'])){
			echo '<script src="dist/js/dashboard-data.js"></script>';

			unset($_SESSION['registro_completo']);
		}
	?>
</body>

</html>
