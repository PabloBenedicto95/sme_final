/*SweetAlert Init*/

$(function() {
	"use strict";
	
	var SweetAlert = function() {};

    //examples 
    SweetAlert.prototype.init = function() {
        
    //Basic
    $('#sa-basic').on('click',function(e){
	    swal({   
			title: "Here's a message!",   
            confirmButtonColor: "#0098a3",   
        });
		return false;
    });

    //A title with a text under
    $('#sa-title').on('click',function(e){
	    swal({   
			title: "Here's a message!",   
            text: "Lorem ipsum dolor sit amet",
			confirmButtonColor: "#0098a3",   
        });
		return false;
    });

    //Success Message
	$('#sa-success').on('click',function(e){
        swal({   
			title: "good job!",   
             type: "success", 
			text: "Lorem ipsum dolor sit amet",
			confirmButtonColor: "#4aa23c",   
        });
		return false;
    });

    //Warning Message
    $('#sa-warning,.sa-warning').on('click',function(e){
	    swal({   
            title: "¿Estás seguro?",   
            text: "Vas a registrarte en un evento, esta opción no se puede deshacer.",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#f8b32d",   
            confirmButtonText: "Si, regístrame!",   
            closeOnConfirm: false 
        }, function(){   
            swal("Deleted!", "Your imaginary file has been deleted.", "success"); 
        });
		return false;
    });

    //Alerta de modificador de evento
    $('#sa-warning-evento,.sa-warning').on('click',function(e){
        swal({   
            title: "¿Estás seguro?",   
            text: "Los datos que modifiques no se podrán deshacer.",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#f8b32d",   
            confirmButtonText: "Si, modifica los datos!",   
            closeOnConfirm: false 
        }, function(){   
            document.getElementById('datos_nuevos').submit();
        });
        return false;
    });

    //Parameter
	$('#sa-params').on('click',function(e){
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to recover this imaginary file!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#f8b32d",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                swal("Deleted!", "Your imaginary file has been deleted.", "success");   
            } else {     
                swal("Cancelled", "Your imaginary file is safe :)", "error");   
            } 
        });
		return false;
    });

    //Modal para el tratamiento de datos de los clientes
    $('#sa-params-cliente').on('click',function(e){
        swal({   
            title: "¿Estás seguro?",   
            text: "Vas a modificar los datos de un cliente, estos cambios no se podrán deshacer.",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#f8b32d",   
            confirmButtonText: "Si, modifica",   
            cancelButtonText: "No!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                document.getElementById('datos_nuevos').submit();
            } else {     
                swal("Abortado", "No hemos modificado ningún dato. Los datos del cliente están a salvo.", "error"); 
            } 
        });
        return false;
    });

    //Modal para el tratamiento de datos de los usuarios
    $('#sa-params-usuario').on('click',function(e){
        swal({   
            title: "¿Estás seguro?",   
            text: "Vas a modificar tus datos, estos cambios no se podrán deshacer.",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#f8b32d",   
            confirmButtonText: "Si, modificar",   
            cancelButtonText: "No!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                document.getElementById('datos_nuevos').submit();
            } else {     
                swal("Abortado", "No hemos modificado ningún dato. Los datos del usuario están a salvo.", "error"); 
            } 
        });
        return false;
    });

    //Modal para la eliminación de un usuario
    $('#sa-params-usuario-borrar').on('click',function(e){
        swal({   
            title: "¿Estás seguro?",   
            text: "Un usuario va a ser eliminado, esta opción no se puede deshacer.",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#f8b32d",   
            confirmButtonText: "Eliminar",   
            cancelButtonText: "No!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                location.href='borrar_usuario.php';
            } else {     
                swal("Abortado", "No hemos borrado ningún dato. Los datos del usuario están a salvo.", "error"); 
            } 
        });
        return false;
    });

    //Modal para la eliminación de un usuario desde el cliente
    $('#sa-params-borrar-usuario').on('click',function(e){
        swal({   
            title: "¿Estás seguro?",   
            text: "Este usuario va a ser eliminado, esta opción no se puede deshacer.",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#f8b32d",   
            confirmButtonText: "Eliminar",   
            cancelButtonText: "No!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) { 
                location.href=e.target.getAttribute('direccion');
            } else {     
                swal("Abortado", "No hemos borrado ningún dato. Los datos del usuario están a salvo.", "error"); 
            } 
        });
        return false;
    });

    //Custom Image
	$('#sa-image').on('click',function(e){
		swal({   
            title: "John!",   
            text: "Recently joined twitter",   
            imageUrl: "dist/img/user.png" ,
			confirmButtonColor: "#f33923",   
			
        });
		return false;
    });

    //Auto Close Timer
	$('#sa-close').on('click',function(e){
        swal({   
            title: "Auto close alert!",   
            text: "I will close in 2 seconds.",   
            timer: 2000,   
            showConfirmButton: false 
        });
		return false;
    });


    },
    //init
    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert;
	
	$.SweetAlert.init();
});