/*Toast Init*/


$(document).ready(function() {
	"use strict";
	
	$.toast({
		heading: 'Email verificado',
        text: 'Te hemos enviado un email con la nueva contraseña',
        position: 'bottom-right',
        loaderBg:'#7FDA44',
        icon: 'success',
        hideAfter: 8500
	});
	
});
          
