<!DOCTYPE html>
<html lang="en">
<head>
	<?php
	session_start();

	if(!isset($_SESSION['usuario'])){
		header('Location: ./');
	}

	include 'modelos/Tools.php';

	$enlace=Tools::crearHeaderYConexion();

	if(isset($_COOKIE['fallo_nombre'])){
		$fallo_nombre=$_COOKIE['fallo_nombre'];
		setcookie("fallo_nombre","",(-1),"/");
	}

	?>
	<title>Crear un nuevo evento</title>
</head>

<body>
	<!--Preloader-->
	<?php
	Tools::preloader();
	?>
	<!--/Preloader-->
	<div class="wrapper theme-1-active pimary-color-blue">
		
		<!-- Top Menu Items -->
		<?php
		Tools::crearMenuTop();
		?>
		<!-- /Top Menu Items -->
		
		<!-- Left Sidebar Menu -->
		<?php
		Tools::crearMenuLeft();
		?>
		<!-- /Left Sidebar Menu -->

		<!-- Main Content -->
		<div class="page-wrapper">
			<div class="container-fluid pt-25">
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default card-view">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark">Nuevo evento</h6>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">

									<div class="row">
										<div class="col-md-12">
											<div class="form-wrap">
												<form data-toggle="validator" role="form" method="post" action="anadir_evento.php">
													<?php
													echo '<script type="text/javascript">
													function comprobar(evento){
														var input=evento.target;

														if(input.value.split("").length==0){
															input.style.borderColor="red";
															}else{
																input.style.borderColor="";
															}
														}
														</script>';
														?>
														<div class="form-group">
															<label for="inputName" class="control-label mb-10">Nombre(*)</label>
															<input type="text" class="form-control" id="input_nombre" placeholder="Nombre" name="nombre" required>
														</div>
														<?php
														if(isset($fallo_nombre)){
															echo '<script type="text/javascript">
															document.getElementById("input_nombre").oninput=comprobar;
															document.getElementById("input_nombre").style.borderColor="'.$fallo_nombre.'";
															</script>';
														}
														?>
														<div class="form-group">
															<label for="inputName" class="control-label mb-10">Nombre del sms</label>
															<input type="text" class="form-control" id="input_nombre_sms"  name="nombre_sms" placeholder="SMS">
														</div>
														<?php
														if(isset($fallo_sms)){
															echo '<script type="text/javascript">
															document.getElementById("input_sms").oninput=comprobar;
															document.getElementById("input_sms").style.borderColor="'.$fallo_sms.'";
															</script>';
														}
														?>
														<div class="form-group">
															<label for="inputName" class="control-label mb-10">Link de la imagen</label>
															<input type="text" class="form-control" id="input_imagen"  name="imagen" placeholder="Imagen">
														</div>
														<?php
														if(isset($fallo_imagen)){
															echo '<script type="text/javascript">
															document.getElementById("input_imagen").oninput=comprobar;
															document.getElementById("input_imagen").style.borderColor="'.$fallo_imagen.'";
															</script>';
														}
														?>
														<div class="form-group">
															<label for="inputName" class="control-label mb-10">Lugar</label>
															<input type="text" class="form-control" id="input_lugar"  name="lugar" placeholder="Lugar">
														</div>
														<?php
														if(isset($fallo_lugar)){
															echo '<script type="text/javascript">
															document.getElementById("input_lugar").oninput=comprobar;
															document.getElementById("input_lugar").style.borderColor="'.$fallo_lugar.'";
															</script>';
														}
														?>
														<div class="form-group">
															<label for="inputName" class="control-label mb-10">Fecha y hora</label>
															<input type="datetime-local" class="form-control fecha" id="input_fecha"  name="fecha" title="Introduce la fecha y la hora">
														</div>
														<?php
														if(isset($fallo_fecha)){
															echo '<script type="text/javascript">
															document.getElementById("input_fecha").oninput=comprobar;
															document.getElementById("input_fecha").style.borderColor="'.$fallo_fecha.'";
															alert(\'Fallo con la fecha\');
															</script>';
														}
														?>
														<div class="form-group">
															<label for="inputName" class="control-label mb-10">Mostrar comprobar puelsera</label>
															<input type="checkbox" class="js-switch js-switch-1" data-color="#2ECC40" id="input_mostrar_comprobar_pulsera"  name="mostrar_comprobar_pulsera" style="width: 5%;">
														</div>
														<?php
														if(isset($fallo_mostrar_comprobar_pulsera)){
															echo '<script type="text/javascript">
															document.getElementById("input_mostrar_comprobar_pulsera").oninput=comprobar;
															document.getElementById("input_mostrar_comprobar_pulsera").style.borderColor="'.$fallo_mostrar_comprobar_pulsera.'";
															</script>';
														}
														?>
														<div class="form-group">
															<label for="inputName" class="control-label mb-10">Registro previo</label>
															<input type="checkbox" class="js-switch js-switch-1" data-color="#2ECC40" id="input_registro_previo"  name="registro_previo" style="width: 5%;">
														</div>
														<?php
														if(isset($fallo_registro_previo)){
															echo '<script type="text/javascript">
															document.getElementById("input_registro_previo").oninput=comprobar;
															document.getElementById("input_registro_previo").style.borderColor="'.$fallo_registro_previo.'";
															</script>';
														}
														?>
														<div class="form-group">
															<label for="inputName" class="control-label mb-10">Registro por email</label>
															<input type="checkbox" class="js-switch js-switch-1" data-color="#2ECC40" id="input_registro_email"  name="registro_email" style="width: 5%;">
														</div>
														<?php
														if(isset($fallo_registro_email)){
															echo '<script type="text/javascript">
															document.getElementById("input_registro_email").oninput=comprobar;
															document.getElementById("input_registro_email").style.borderColor="'.$fallo_registro_email.'";
															</script>';
														}
														?>
														<div class="form-group">
															<label for="inputName" class="control-label mb-10">Registro telefónico</label>
															<input type="checkbox" class="js-switch js-switch-1" data-color="#2ECC40" id="input_registro_telefono"  name="registro_telefono" style="width: 5%;">
														</div>
														<?php
														if(isset($fallo_registro_telefono)){
															echo '<script type="text/javascript">
															document.getElementById("input_registro_telefono").oninput=comprobar;
															document.getElementById("input_registro_telefono").style.borderColor="'.$fallo_registro_telefono.'";
															</script>';
														}
														?>
														<div class="form-group">
															<label for="inputName" class="control-label mb-10">Id del lugar de Facebook</label>
															<input type="text" class="form-control" id="input_id_lugar_fb"  name="id_lugar_fb" placeholder="Id de fb">
														</div>
														<?php
														if(isset($fallo_id_lugar_fb)){
															echo '<script type="text/javascript">
															document.getElementById("input_id_lugar_fb").oninput=comprobar;
															document.getElementById("input_id_lugar_fb").style.borderColor="'.$fallo_id_lugar_fb.'";
															</script>';
														}
														?>
														<div class="form-group">
															<label for="inputName" class="control-label mb-10">Página de Facebook</label>
															<input type="text" class="form-control" id="input_facebook_pagina"  name="facebook_pagina" placeholder="Página facebook">
														</div>
														<?php
														if(isset($fallo_facebook_pagina)){
															echo '<script type="text/javascript">
															document.getElementById("input_facebook_pagina").oninput=comprobar;
															document.getElementById("input_facebook_pagina").style.borderColor="'.$fallo_facebook_pagina.'";
															</script>';
														}
														?>
														<div class="form-group">
															<label for="inputName" class="control-label mb-10">URL</label>
															<input type="text" class="form-control" id="input_url"  name="url" placeholder="url">
														</div>
														<?php
														if(isset($fallo_url)){
															echo '<script type="text/javascript">
															document.getElementById("input_url").oninput=comprobar;
															document.getElementById("input_url").style.borderColor="'.$fallo_url.'";
															</script>';
														}
														?>

														<div class="form-group" <?php
														if(!isset($_SESSION['admin'])){
															echo 'style="display: none;"';
														}
														?>>
															<label for="inputName" class="control-label mb-10">Cliente</label>
															<select class="form-control" id="exampleInputuname_1" name="id_cliente">
																<option value="">--Ninguno--</option>
																<?php
																$consulta=mysqli_query($enlace, 'select * from sme_clientes');

																while($fila=mysqli_fetch_array($consulta)){
																	echo '<option value="'.$fila['ID'].'"';
																	if($_SESSION['id_cliente']==$fila['ID']){
																		echo 'selected';
																	}
																	echo '>'.$fila['nombre'].'</option>';
																}
																?>
															</select>
														</div>


														<div class="form-group mb-0">
															<button type="submit" class="btn btn-success btn-anim"><i class="icon-rocket"></i><span class="btn-text">Crear evento</span></button>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Footer -->
					<?php
					Tools::crearFooter();
					?>
					<!-- /Footer -->
				</div>
				
				<!-- /Main Content -->

			</div>
			<!-- /#wrapper -->

			<!-- JavaScript -->

			<!-- jQuery -->
			<script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>

			<!-- Bootstrap Core JavaScript -->
			<script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

			<!-- Data table JavaScript -->
			<script src="vendors/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>

			<!-- Slimscroll JavaScript -->
			<script src="dist/js/jquery.slimscroll.js"></script>

			<!-- Progressbar Animation JavaScript -->
			<script src="vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
			<script src="vendors/bower_components/jquery.counterup/jquery.counterup.min.js"></script>

			<!-- Fancy Dropdown JS -->
			<script src="dist/js/dropdown-bootstrap-extended.js"></script>

			<!-- Sparkline JavaScript -->
			<script src="vendors/jquery.sparkline/dist/jquery.sparkline.min.js"></script>

			<!-- Owl JavaScript -->
			<script src="vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>

			<!-- Switchery JavaScript -->
			<script src="vendors/bower_components/switchery/dist/switchery.min.js"></script>

			<!-- EChartJS JavaScript -->
			<script src="vendors/bower_components/echarts/dist/echarts-en.min.js"></script>
			<script src="vendors/echarts-liquidfill.min.js"></script>

			<!-- Scripts para los datapickers y los modales-->
			<!-- Moment JavaScript -->
			<script type="text/javascript" src="vendors/bower_components/moment/min/moment-with-locales.min.js"></script>

			<!-- Bootstrap Colorpicker JavaScript -->
			<script src="vendors/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>

			<!-- Switchery JavaScript -->
			<script src="vendors/bower_components/switchery/dist/switchery.min.js"></script>

			<!-- Select2 JavaScript -->
			<script src="vendors/bower_components/select2/dist/js/select2.full.min.js"></script>

			<!-- Bootstrap Select JavaScript -->
			<script src="vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

			<!-- Bootstrap Tagsinput JavaScript -->
			<script src="vendors/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

			<!-- Bootstrap Touchspin JavaScript -->
			<script src="vendors/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"></script>

			<!-- Multiselect JavaScript -->
			<script src="vendors/bower_components/multiselect/js/jquery.multi-select.js"></script>


			<!-- Bootstrap Switch JavaScript -->
			<script src="vendors/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>

			<!-- Bootstrap Datetimepicker JavaScript -->
			<script type="text/javascript" src="vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

			<!-- Form Advance Init JavaScript -->
			<script src="dist/js/form-advance-data.js"></script>

			<!-- Slimscroll JavaScript -->
			<script src="dist/js/jquery.slimscroll.js"></script>

			<!-- Fancy Dropdown JS -->
			<script src="dist/js/dropdown-bootstrap-extended.js"></script>

			<!-- Owl JavaScript -->
			<script src="vendors/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>

			<!-- Toast JavaScript -->
			<script src="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>

			<!-- Init JavaScript -->
			<script src="dist/js/init.js"></script>
			<?php
			if(isset($_SESSION['registro_completo'])){
				echo '<script src="dist/js/dashboard-data.js"></script>';

				unset($_SESSION['registro_completo']);
			}
			?>
		</body>
		</html>
