<?php
session_start();

if(!isset($_SESSION['usuario']) || !isset($_SESSION['evento'])){
	if(!isset($_SESSION['evento'])){
		header('Location: ./');
	}

	if(!isset($_SESSION['usuario'])){
		$_SESSION['registro_rapido']=(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		header('Location: ./login.php');
	}
}else{
	$todoOK=true;

	include('modelos/Tools.php');
	$enlace = Tools::conexion();
	//$accion=mysqli_query($enlace, 'insert into usuarios_en_eventos (id_evento, id_usuario) values ("'.$id_evento.'", "'.$_SESSION['id_usuario'].'")');
	if($_POST['contrasena1']!=$_POST['contrasena2']){
		setcookie("fallo_contrasena","red",time()+60*60*24*365,"/");
		header('Location: ./registrarse.php');
		$todoOK=false;
	}else{
		$contrasena=mysqli_real_escape_string($enlace, $_POST['contrasena1']);
	}

	if(isset($_POST['email'])){
		if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
			$email=mysqli_real_escape_string($enlace, filter_var($_POST['email'], FILTER_SANITIZE_EMAIL));
		}else{
			setcookie("fallo_email","red",time()+60*60*24*365,"/");
			header('Location: ./registrarse.php');
			$todoOK=false;
		}
	}

	if(isset($_POST['telefono'])){
		if(filter_var($_POST['telefono'], FILTER_VALIDATE_INT)){
			$telefono=mysqli_real_escape_string($enlace, filter_var($_POST['telefono'], FILTER_SANITIZE_NUMBER_INT));
		}else{
			setcookie("fallo_telefono","red",time()+60*60*24*365,"/");
			header('Location: ./registrarse.php');
			$todoOK=false;
		}
	}

	if($todoOK==true){
		$id_evento=$_SESSION['evento']['ID'];

		$consulta='insert into usuarios_en_eventos (id_evento, id_usuario, registro_previo';

		if(isset($email)){
			$consulta.=', mail_registro';
		}

		if(isset($telefono)){
			$consulta.=', telefono';
		}

		$consulta.=') values ("'.$id_evento.'", "'.$_SESSION['id_usuario'].'", 1';

		if(isset($email)){
			$consulta.=', "'.$email.'"';
			unset($email);
		}

		if(isset($telefono)){
			$consulta.=', "'.$telefono.'"';
			unset($telefono);
		}

		$consulta.=')';

		$accion=mysqli_query($enlace, $consulta);
		echo $consulta;

		$_SESSION['registro_completo']='Te has registrado en el evento \''.$_SESSION['evento']['nombre'].'\'';

		unset($_SESSION['evento']);

		header('Location: ./');
	}
}
?>