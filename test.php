<?php
//Gráfico 'Número de publicaciones'
session_start();
include 'modelos/Tools.php';
$enlace=Tools::conexion();
$numero=11;

$array_publicaciones=array();

$tabla_resultado=mysqli_query($enlace, 'SELECT * FROM sme_acciones WHERE id_evento='.$numero); 

$intervalo=60;
while($fila=mysqli_fetch_array($tabla_resultado)){
	$consulta='SELECT COUNT(*) AS cantidad, UNIX_TIMESTAMP(sme_publicaciones.hora) DIV ('.$intervalo.' * 60) AS timekey FROM sme_publicaciones WHERE id_accion='.$fila['ID'].' GROUP BY timekey ORDER BY timekey';
	$cache=mysqli_query($enlace, $consulta);

	array_push($array_publicaciones, $cache);
}

$mostrar=array();

foreach ($array_publicaciones as $key => $value) {
	while($fila=mysqli_fetch_array($value)){
		array_push($mostrar, array('cantidad'=>$fila['cantidad'], 'timekey'=>$fila['timekey']));
	}
}

$cache=array();
foreach ($mostrar as $key => $value) {
	array_push($cache, array(intval($value['timekey']), intval($value['cantidad'])));
}
$json_datos=json_encode($cache);

foreach ($cache as $key => $value) {
	foreach ($value as $llave => $valor) {
		if($llave==0){
			echo gmdate("Y-m-d H:i:s", $valor);
		}else{
			echo $valor;
		}
		echo '<br>';
	}
	echo '<hr>';
}

?>