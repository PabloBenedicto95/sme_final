<!DOCTYPE html>
<html lang="en">
<head>
	<?php
	session_start();
	include 'modelos/Tools.php';

	$enlace=Tools::crearHeaderYConexion();

	if(!isset($_GET['id'])){
		header('Location: ./');
	}

	
	if($_GET['id']!=$_SESSION['id_cliente'] and !isset($_SESSION['admin'])){
		header('Location: ./');
	}

	$_SESSION['cache_url']=(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

	$numero=mysqli_real_escape_string($enlace, filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT));
	$consulta=mysqli_query($enlace, 'select * from sme_clientes where ID='.$numero);

	$fila=mysqli_fetch_assoc($consulta);
	$cliente=$fila;
	$_SESSION['cache_client']=$cliente;

	if(isset($_COOKIE['error_log'])){
		$error_log=1;
		setcookie("error_log","",(-1),"/");
	}

	if(isset($_SESSION['admin'])){
		setcookie("cookie_id_cliente",$numero,time()+60*60*24*365,"/");
	}

	$_SESSION['ultima_pagina_borrar_usuario']=(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

	?>
	<title><?php
	echo $cliente['nombre'];
	?> I Clientes</title>

	<!--alerts CSS -->
	<link href="vendors/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css">

	<!-- Fancy-Buttons CSS -->
	<link href="dist/css/fancy-buttons.css" rel="stylesheet" type="text/css">
</head>

<body>
	<!--Preloader-->
	<?php
	Tools::preloader();
	?>
	<!--/Preloader-->
	<div class="wrapper theme-1-active pimary-color-blue">

		<!-- Top Menu Items -->
		<?php
		Tools::crearMenuTop();
		?>
		<!-- /Top Menu Items -->
		
		<!-- Left Sidebar Menu -->
		<?php
		Tools::crearMenuLeft();
		?>
		<!-- /Left Sidebar Menu -->	
		
		

		<!-- Main Content -->
		<div class="page-wrapper">
			<div class="container-fluid pt-25">

				<!-- Row -->
				<div class="row">
					<div class="col-lg-3 col-xs-12">
						<div class="panel panel-default card-view  pa-0">
							<div class="panel-wrapper collapse in">
								<div class="panel-body  pa-0">
									<div class="profile-box">
										<div class="profile-cover-pic" style="background-image: url('dist/img/detras.png'); background-size: 100%;">
											<!--<img src="dist/img/detras.png" style="width: 100%;">-->
											<div class="profile-image-overlay" style="background-color: black">
											</div>
										</div>
										<div class="profile-info text-center">
											<div class="profile-img-wrap">
												<img class="inline-block mb-10" src="dist/img/desconocido.jpg" alt="user"/>
											</div>	
											<h5 class="block mt-10 mb-5 weight-500 capitalize-font txt-info" style="color: #00469d !important;"><?php
											echo $_SESSION['cache_client']['nombre'];
											?></h5>
											<h6 class="block capitalize-font pb-20">Información de cliente</h6>
										</div>	
										<div class="social-info">
											<button class="btn btn-info btn-block  btn-anim mt-30" data-toggle="modal" data-target="#myModal" id="boton_editar"><i class="fa fa-pencil"></i><span class="btn-text">Editar información</span></button>
											<div id="myModal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
												<div class="modal-dialog">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
															<h5 class="modal-title" id="myModalLabel">Editar cliente</h5>
														</div>
														<div class="modal-body">
															<!-- Row -->
															<div class="row">
																<div class="col-lg-12">
																	<div class="">
																		<div class="panel-wrapper collapse in">
																			<div class="panel-body pa-0">
																				<div class="col-sm-12 col-xs-12">
																					<div class="form-wrap">
																						<form action="cambiar_datos_cliente.php" method="post" id="datos_nuevos">
																							<div class="form-body overflow-hide">
																								<div class="form-group">
																									<label class="control-label mb-10" for="exampleInputuname_1">Nombre</label>
																									<div class="input-group">
																										<div class="input-group-addon"><i class="icon-user"></i></div>
																										<input type="text" class="form-control" id="exampleInputuname_1" value="<?php
																										echo $_SESSION['cache_client']['nombre'];
																										?>" name="nombre" required>
																									</div>
																								</div>
																								<?php

																								echo '<div class="form-group">
																								<label class="control-label mb-10" for="exampleInputEmail_1">Contacto de email</label>
																								<div class="input-group">
																								<div class="input-group-addon"><i class="icon-envelope-open"></i></div>
																								<input type="email" class="form-control" id="exampleInputEmail_1" ';
																								if($_SESSION['cache_client']['email']!=''){
																									echo 'value="'.$_SESSION['cache_client']['email'].'"';
																								}
																								echo ' name="email">
																								</div>
																								</div>

																								<div class="form-group">
																								<label class="control-label mb-10" for="exampleInputContact_1">Número de teléfono</label>
																								<div class="input-group">
																								<div class="input-group-addon"><i class="icon-phone"></i></div>
																								<input type="text" data-mask="(999) 999-999" class="form-control" id="telefono" name="telefono"';
																								if($_SESSION['cache_client']['telefono']!=''){
																									echo 'value="'.$_SESSION['cache_client']['telefono'].'"';
																								}


																								echo ' name="telefono">
																								</div>
																								</div>

																								<script type="text/javascript">
																								document.getElementById(\'telefono\').oninput=cambioNumero;

																								function cambioNumero(evento){
																									var input=evento.target;

																									input.value=parseInt(input.value);
																									if(input.value==\'NaN\'){
																										input.value=\'\';
																									}

																									var array=input.value.split(\'\');

																									if(array.length==9){
																										var mostrar=\'\';
																										for(var i=0; i<3; i++){
																											mostrar+=array[i];
																										}
																										mostrar+=\'\';

																										for(var i=3; i<9; i++){
																											mostrar+=array[i];
																											if(i==5){
																												mostrar+=\'\';
																											}
																										}
																										input.value=mostrar;
																										}else if(array.length>9){
																											var mostrar=\'\';
																											for(var i=0; i<9; i++){
																												mostrar+=array[i];
																											}
																											input.value=mostrar;
																											cambioNumero(evento);
																										}
																									}
																									</script>';
																									?>
																								</div>	
																							</form>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="modal-footer">
																<script type="text/javascript">
																	
																	document.cookie='retorno='+window.location.href;
																	document.cookie='cache_cliente=<?php
																	echo Tools::encriptar(json_encode($_SESSION['cache_client'], TRUE));
																	?>';

																</script>
																<button class="btn btn-success waves-effect" data-dismiss="modal" alt="alert" id="sa-params-cliente">Guardar cambios</button>
																<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancelar</button>
															</div>
														</div>
														<!-- /.modal-content -->
													</div>
													<!-- /.modal-dialog -->
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-9 col-xs-12">
							<div class="panel panel-default card-view pa-0">
								<div class="panel-wrapper collapse in">
									<div  class="panel-body pb-0">
										<div  class="tab-struct custom-tab-1">
											<ul role="tablist" class="nav nav-tabs nav-tabs-responsive" id="myTabs_8">
												<li class="active" role="presentation"><a  data-toggle="tab" id="profile_tab_8" role="tab" href="#profile_8" aria-expanded="false"><span>Datos</span></a></li>
											</ul>
											<div class="tab-content" id="myTabContent_8">
												<div class="row">
													<div class="col-lg-12">
														<div class="">
															<div class="panel-wrapper collapse in">
																<div class="panel-body pa-0">
																	<div class="col-sm-12 col-xs-12">
																		<div class="form-wrap">
																			<form action="#">
																				<div class="form-body overflow-hide">
																					<div class="form-group">
																						<label class="control-label mb-10" for="exampleInputuname_1">Nombre</label>
																						<div class="input-group">
																							<div class="input-group-addon"><i class="icon-user"></i></div>
																							<input type="text" class="form-control" id="exampleInputuname_1" value="<?php
																							echo $_SESSION['cache_client']['nombre'];
																							?>" readonly style="background-color: ">
																						</div>
																					</div>
																					<?php

																					if($_SESSION['cache_client']['email']!=''){
																						echo '<div class="form-group">
																						<label class="control-label mb-10" for="exampleInputEmail_1">Contacto de email</label>
																						<div class="input-group">
																						<div class="input-group-addon"><i class="icon-envelope-open"></i></div>
																						<input type="email" class="form-control" id="exampleInputEmail_1" value="'.$_SESSION['cache_client']['email'].'" required readonly style="background-color: ">
																						</div>
																						</div>';
																					}

																					if($_SESSION['cache_client']['telefono']!=''){
																						echo '<div class="form-group">
																						<label class="control-label mb-10" for="exampleInputContact_1">Número de teléfono</label>
																						<div class="input-group">
																						<div class="input-group-addon"><i class="icon-phone"></i></div>
																						<input type="text" data-mask="(999) 999-999" class="form-control" id="telefono" name="telefono" value="'.$_SESSION['cache_client']['telefono'].'" required readonly style="background-color: ">
																						</div>
																						</div>

																						<script type="text/javascript">
																						document.getElementById(\'telefono\').oninput=cambioNumero;

																						function cambioNumero(evento){
																							var input=evento.target;

																							input.value=parseInt(input.value);
																							if(input.value==\'NaN\'){
																								input.value=\'\';
																							}

																							var array=input.value.split(\'\');

																							if(array.length==9){
																								var mostrar=\'\';
																								for(var i=0; i<3; i++){
																									mostrar+=array[i];
																								}
																								mostrar+=\'\';

																								for(var i=3; i<9; i++){
																									mostrar+=array[i];
																									if(i==5){
																										mostrar+=\'\';
																									}
																								}
																								input.value=mostrar;
																								}else if(array.length>9){
																									var mostrar=\'\';
																									for(var i=0; i<9; i++){
																										mostrar+=array[i];
																									}
																									input.value=mostrar;
																									cambioNumero(evento);
																								}
																							}
																							</script>';
																						}
																						?>
																					</div>			
																				</form>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-default card-view">
									<div class="panel-heading">
										<div class="pull-left">
											<h6 class="panel-title txt-dark">Usuarios de <?php
											echo $cliente['nombre'];
											?></h6>
										</div>
										<a href="anadir_usuario.php" style="float: right; height: 25%;"><button class="btn btn-warning btn-icon-anim btn-square" title="Añadir usuario"><i class="icon-user-follow"></i></button></a>
										<div class="clearfix"></div>
									</div>
									<div class="panel-wrapper collapse in">
										<div class="panel-body">
											<div class="panel-group accordion-struct accordion-style-1" id="accordion_2" role="tablist" aria-multiselectable="true">
												<?php
												$contador=1;
												$consulta=mysqli_query($enlace, 'SELECT * FROM sme_usuarios WHERE id_cliente='.$cliente['ID']);

												while($fila=mysqli_fetch_array($consulta)){
													echo '<div class="panel panel-default">
													<div class="panel-heading" role="tab" id="heading_'.$contador.'">
													<a role="button" data-toggle="collapse" data-parent="#accordion_2" href="#collapse_'.$contador.'" aria-expanded="false" class="collapsed"><div class="icon-ac-wrap pr-20"><span class="plus-ac"><i class="ti-plus"></i></span><span class="minus-ac"><i class="ti-minus"></i></span></div>'.$fila['nombre'].'</a> 
													</div>
													<div id="collapse_'.$contador.'" class="panel-collapse collapse" role="tabpanel" aria-expanded="false" style="height: 0px;">
													<div class="panel-body pa-15">
													<button class="btn btn-success btn-outline fancy-button btn-0" style="float: left;" onclick="location.href=\'usuario.php?id='.$fila['ID'].'\'">Ver datos</button>
													';
													if($_SESSION['id']!=$fila['ID']){
														echo '<button class="btn btn-danger btn-outline fancy-button btn-0" style="float: left; margin-left: 5%;" direccion="borrar_usuario.php?id='.$fila['ID'].'" data-dismiss="modal" alt="alert" id="sa-params-borrar-usuario">Eliminar</button>';
													}
													echo '</div>
													</div>
													</div>';
													$contador++;
												}
												?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-default card-view">
									<div class="panel-heading">
										<div class="pull-left">
											<h6 class="panel-title txt-dark">Eventos de <?php
											echo $cliente['nombre'];
											?></h6>
										</div>
										<a href="nuevo_evento.php" style="float: right; height: 25%;">
											<button class="btn btn-warning btn-icon-anim btn-square" title="Crear un evento">
												<div data-icon="H" class="linea-icon linea-elaborate"></div>
											</button>
										</a>
										<div class="clearfix"></div>
									</div>
									<div class="panel-wrapper collapse in">
										<div class="panel-body">
											<?php
											$consulta=mysqli_query($enlace, 'SELECT * FROM sme_eventos WHERE id_cliente='.$cliente['ID']);
											Tools::cartel_eventos($consulta);
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->
					<!-- Footer -->
					<?php
					Tools::crearFooter();
					?>
					<!-- /Footer -->
				</div>
			</div>
			<!-- /Main Content -->

		</div>
	</div>
	<!-- /Main Content -->

</div>
<!-- /#wrapper -->

<!-- JavaScript -->
<?php
Tools::crearJavascript();
?>
<!-- Sweet-Alert  -->
<script src="vendors/bower_components/sweetalert/dist/sweetalert.min.js"></script>	
<script src="vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
<script src="dist/js/sweetalert-data.js"></script>

<?php
if(isset($error_log)){
	echo '<script src="dist/js/toast-data.js"></script>';
	unset($error_log);
}

if(isset($_SESSION['registro_completo'])){
	echo '<script src="dist/js/dashboard-data_eliminar_user.js"></script>';

	unset($_SESSION['registro_completo']);
}
?>


</body>

</html>
